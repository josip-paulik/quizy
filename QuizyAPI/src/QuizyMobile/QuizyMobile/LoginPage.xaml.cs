﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using QuizyMobile.Services;
using QuizyMobile.Utility;
using QuizyMobile.Utility.Enums;
using QuizyMobile.ViewModels;
using Xamarin.Forms;

namespace QuizyMobile
{
    public partial class LoginPage : ContentPage
    {
        private readonly IQuizyAPIService quizyAPIService;
        public RegisterModel RegisterModel { get; set; }

        public LoginPage()
        {
            InitializeComponent();
            RegisterModel = new RegisterModel();
            BindingContext = RegisterModel;
            NavigationPage.SetHasBackButton(this, false);
            quizyAPIService = App.IoCContainer.GetInstance<IQuizyAPIService>();
        }

        async void registerButton_Clicked(System.Object sender, System.EventArgs e)
        {
            loginSummaryStackLayout.Children.Clear();

            RegisterModel.InstallationId = CreateInstallationId();
            RegisterModel.Role = (int)RoleEnum.Student;

            var errorMessages = GetErrorMessagesFromValidation(RegisterModel); 
            if (!errorMessages.IsNullOrEmpty())
            {
                foreach (var errorMessage in errorMessages)
                {
                    var label = new Label
                    {
                        Text = errorMessage,
                        TextColor = Color.IndianRed,
                        HorizontalTextAlignment = TextAlignment.Start
                    };
                    loginSummaryStackLayout.Children.Add(label);       
                }
            }
            else
            {
                var token = new TokenModel();
                try
                {
                    token = quizyAPIService.Register(RegisterModel);
                }
                catch (UserAlreadyExistsException ex)
                {
                    var label = new Label
                    {
                        Text = "User with this email or unique academic id already exists.",
                        TextColor = Color.IndianRed,
                        HorizontalTextAlignment = TextAlignment.Start
                    };
                    loginSummaryStackLayout.Children.Add(label);
                    return;
                }
                catch (Exception ex)
                {
                    await DisplayAlert("Error", "Error while registering", "OK");
                    return;
                }

                var userData = new UserData
                {
                    EmailAddress = RegisterModel.EmailAddress,
                    FullName = RegisterModel.FullName,
                    InstallationId = RegisterModel.InstallationId,
                    Token = token.Token,
                    UniqueAcademicId = RegisterModel.UniqueAcademicId
                };

                Application.Current.Properties.Add("emailAddress", RegisterModel.EmailAddress);
                Application.Current.Properties.Add("uniqueAcademicId", RegisterModel.UniqueAcademicId);
                Application.Current.Properties.Add("installationId", RegisterModel.InstallationId);
                Application.Current.Properties.Add("token", token.Token);
                Application.Current.Properties.Add("fullName", RegisterModel.FullName);
                await Application.Current.SavePropertiesAsync();
                await Navigation.PushAsync(new MainPage());
            }
        }

        /// <summary>
        /// Performs validation on register model and returns collection of labels.
        /// Each label in collection represents an error in register model.
        /// </summary>
        /// <param name="registerModel">Model to be validated</param>
        /// <returns>Collection of model error messages if there are any.</returns>
        private List<string> GetErrorMessagesFromValidation(RegisterModel registerModel)
        {
            var returnCollection = new List<string>();

            // Added due to data annotations inablity to analyze email format properly.
            // If data annotaion for email address is fixed, remove this validation
            if (registerModel != null && registerModel.EmailAddress != string.Empty)
            {
                var emailAttribute = new EmailAddressAttribute();

                if (!emailAttribute.IsValid(registerModel.EmailAddress))
                {
                    returnCollection.Add("Email address is in wrong format.");
                }
            }

            var errorCollection = new List<ValidationResult>();
            if (!Validator.TryValidateObject(registerModel, new ValidationContext(registerModel, null, null), errorCollection))
            {
                foreach (var error in errorCollection)
                {
                    returnCollection.Add(error.ErrorMessage);
                }
            }

            return returnCollection;
        }

        /// <summary>
        /// Creates installation id which is equivalent
        /// to password in Quizy.
        /// </summary>
        /// <returns>Installation id</returns>
        private string CreateInstallationId()
        {
            var guid = Guid.NewGuid().ToString();

            return guid;
        }
    }
}
