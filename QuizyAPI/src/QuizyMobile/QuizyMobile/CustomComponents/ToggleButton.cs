﻿using System;
using Xamarin.Forms;

namespace QuizyMobile.CustomComponents
{
    public class ToggleButton : Button
    {
        public bool IsSelected { get; set; }

        public Color SecondaryBackgroundColor { get; set; }
        public Color SecondaryTextColor { get; set; }

        public ToggleButton()
        {
            IsSelected = false;

            BackgroundColor = Color.White;
            TextColor = Color.Olive;

            SecondaryBackgroundColor = Color.Olive;
            SecondaryTextColor = Color.White;

            Clicked += ToggleColor;
        }

        public void ToggleColor(object sender, EventArgs e)
        {
            var color = BackgroundColor;
            BackgroundColor = SecondaryBackgroundColor;
            SecondaryBackgroundColor = color;

            color = TextColor;
            TextColor = SecondaryTextColor;
            SecondaryTextColor = color;

            IsSelected = !IsSelected;
        }
    }
}
