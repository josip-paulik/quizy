﻿using System;
namespace QuizyMobile.Utility.Enums
{
    /// <summary>
    /// Enumerates question types that question can have.
    /// </summary>
    public enum QuestionTypeEnum
    {
        Choice = 1,
        OpenText = 2
    }
}
