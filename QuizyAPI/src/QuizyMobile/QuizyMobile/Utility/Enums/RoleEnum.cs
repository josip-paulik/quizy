﻿using System;
namespace QuizyMobile.Utility.Enums
{
    /// <summary>
    /// Enumerates user roles in Quizy system.
    /// </summary>
    public enum RoleEnum
    {
        Professor = 1,
        Student = 2
    }
}

