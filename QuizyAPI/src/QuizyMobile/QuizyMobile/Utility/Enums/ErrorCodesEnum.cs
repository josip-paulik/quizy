﻿using System;
namespace QuizyMobile.Utility
{
    /// <summary>
    /// This enum assignes number to various errors
    /// that can happen on an API request.
    /// </summary>
    public enum ErrorCodesEnum
    {
        UserAlreadyExists = 1,
        BadRequest = 2,
        UserDoesNotExist = 3,
        GroupAlreadyHasExams = 4
    }
}
