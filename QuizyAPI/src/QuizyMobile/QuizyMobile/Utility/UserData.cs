﻿using System;
namespace QuizyMobile.Utility
{
    public class UserData
    {
        public string Token { get; set; }

        public string EmailAddress { get; set; }
   
        public string FullName { get; set; }
        
        public string UniqueAcademicId { get; set; }
        
        public string InstallationId { get; set; }
    }
}
