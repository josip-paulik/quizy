﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QuizyMobile.Utility
{
    public static class Extensions
    {
        /// <summary>
        /// Checks if list is null or empty.
        /// </summary>
        /// <typeparam name="T">Object type of list element</typeparam>
        /// <param name="enumerable">List that is checked</param>
        /// <returns>True if list is null or empty.</returns>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            var returnValue = enumerable == null || enumerable.Count() == 0;

            return returnValue;
        }
    }
}
