﻿using System;
using System.Collections.Generic;
using QuizyMobile.Services;
using QuizyMobile.Utility;
using Xamarin.Forms;

namespace QuizyMobile
{
    public partial class TestPerfomanceDetailsPage : ContentPage
    {
        private readonly IQuizyAPIService quizyAPIService;
        private readonly int testId;

        public TestPerfomanceDetailsPage(int testId)
        {
            InitializeComponent();
            this.testId = testId;

            quizyAPIService = App.IoCContainer.GetInstance<IQuizyAPIService>();
        }

        protected override void OnAppearing()
        {
            FillTestPerfomanceDetailsScroll();
        }

        private void FillTestPerfomanceDetailsScroll()
        {
            var testPerfomanceDetailsData = quizyAPIService.GetStudentTestPerfomanceDetails(testId);

            var allTestPerfomancesLayout = new StackLayout();
            foreach (var testPerfomanceDetail in testPerfomanceDetailsData)
            {
                var testPerfomanceLayout = new StackLayout();

                var questionLabel = new Label
                {
                    Text = $"{testPerfomanceDetail.QuestionIndex}. {testPerfomanceDetail.QuestionText}"
                };
                testPerfomanceLayout.Children.Add(questionLabel);

                if (!testPerfomanceDetail.OfferedAnswers.IsNullOrEmpty())
                {
                    var offeredAnswerLayout = new StackLayout();
                    offeredAnswerLayout.Children.Add(new Label
                    {
                        Text = "Offered answers:"
                    });
                    foreach (var offeredAnswer in testPerfomanceDetail.OfferedAnswers)
                    {
                        var label = new Label
                        {
                            Text = offeredAnswer,
                            FontAttributes = FontAttributes.Bold
                        };
                        offeredAnswerLayout.Children.Add(label);
                    }
                    testPerfomanceLayout.Children.Add(offeredAnswerLayout);
                }

                var givenAnswerLayout = new StackLayout();
                givenAnswerLayout.Children.Add(new Label
                {
                    Text = "Given answers:"
                });
                if (!testPerfomanceDetail.GivenAnswers.IsNullOrEmpty())
                {
                    foreach (var givenAnswer in testPerfomanceDetail.GivenAnswers)
                    {
                        var label = new Label
                        {
                            Text = givenAnswer,
                            TextColor = testPerfomanceDetail.CorrectAnswers.Contains(givenAnswer)
                                ? Color.Olive : Color.IndianRed
                        };
                        givenAnswerLayout.Children.Add(label);
                    }
                }
                else
                {
                    var label = new Label
                    {
                        Text = "You did not give any answer.",
                        TextColor = Color.IndianRed
                    };
                    givenAnswerLayout.Children.Add(label);
                }
                
                testPerfomanceLayout.Children.Add(givenAnswerLayout);

                var correctAnswerLayout = new StackLayout();
                correctAnswerLayout.Children.Add(new Label
                {
                    Text = "Correct answers:"
                });
                foreach (var correctAnswer in testPerfomanceDetail.CorrectAnswers)
                {
                    var label = new Label
                    {
                        Text = correctAnswer,
                        TextColor = Color.Olive
                    };
                    correctAnswerLayout.Children.Add(label);
                }
                testPerfomanceLayout.Children.Add(correctAnswerLayout);

                var pointsScoredLabel = new Label
                {
                    Text = $"Points scored: {testPerfomanceDetail.PointsScored}/{testPerfomanceDetail.MaximumPoints}"
                };
                testPerfomanceLayout.Children.Add(pointsScoredLabel);


                var frame = new Frame
                {
                    Content = testPerfomanceLayout
                };
                allTestPerfomancesLayout.Children.Add(frame);
            }
            testPerfomanceDetailsScroll.Content = allTestPerfomancesLayout;
        }
    }
}
