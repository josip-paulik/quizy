﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using QuizyMobile.Utility;
using QuizyMobile.ViewModels;
using Xamarin.Forms;

namespace QuizyMobile.Services
{
    public class QuizyAPIService : IQuizyAPIService
    {
        private string baseUrl = Device.RuntimePlatform == Device.Android ? "https://10.0.2.2:5001" : "https://localhost:5001";
        private readonly HttpClient client;
        

        public QuizyAPIService()
        {
            var handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;
            client = new HttpClient(handler);
        }

        /// <summary>
        /// Logs in existing user to application.
        /// </summary>
        /// <param name="loginModel">Existing user</param>
        /// <returns>Token model containing token.</returns>
        public TokenModel Login(LoginModel loginModel)
        {
            var requestUrl = $"{baseUrl}/user/login";

            var serializedModel = JsonSerializer.Serialize(loginModel);
            var buffer = System.Text.Encoding.UTF8.GetBytes(serializedModel);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = client.PostAsync(requestUrl, byteContent).Result;
            var deserializedResponse = response.Content.ReadAsStringAsync().Result;

            var serializeOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true
            };

            if (response.StatusCode != HttpStatusCode.OK)
            {
                try
                {
                    var errorModel = JsonSerializer.Deserialize<ErrorModel>(deserializedResponse, serializeOptions);
                    if (errorModel.ErrorCode == (int)ErrorCodesEnum.UserDoesNotExist)
                    {
                        throw new Exception(errorModel.ErrorMessage);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            var token = JsonSerializer.Deserialize<TokenModel>(deserializedResponse, serializeOptions);

            return token;
        }

        /// <summary>
        /// Registers new user to application.
        /// </summary>
        /// <param name="registerModel">New user</param>
        /// <returns>Token model containing token.</returns>
        public TokenModel Register(RegisterModel registerModel)
        {
            var requestUrl = $"{baseUrl}/user/register";

            var serializedModel = JsonSerializer.Serialize(registerModel);
            var buffer = System.Text.Encoding.UTF8.GetBytes(serializedModel);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = client.PostAsync(requestUrl, byteContent).Result;
            var deserializedResponse = response.Content.ReadAsStringAsync().Result;

            var serializeOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true
            };

            if (!response.IsSuccessStatusCode)
            {
                try
                {
                    var errorModel = JsonSerializer.Deserialize<ErrorModel>(deserializedResponse, serializeOptions);
                    if (errorModel.ErrorCode == (int)ErrorCodesEnum.UserAlreadyExists)
                    {
                        throw new UserAlreadyExistsException(errorModel.ErrorMessage);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            var token = JsonSerializer.Deserialize<TokenModel>(deserializedResponse, serializeOptions);

            return token;
        }

        /// <summary>
        /// Tries to join test with provided access code.
        /// </summary>
        /// <param name="accessCode">Access code</param>
        /// <returns>True if user can join the test.</returns>
        public bool CanJoin(string accessCode)
        {
            accessCode = accessCode.ToUpper();
            var requestUrl = $"{baseUrl}/test/canJoin?accessCode={accessCode}";

            var token = Application.Current.Properties["token"] as string;
            if (!token.IsNullOrEmpty())
            {
                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", token);
            }

            var response = client.GetAsync(requestUrl).Result;
            var deserializedResponse = response.Content.ReadAsStringAsync().Result;
            if (!response.IsSuccessStatusCode)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets overall perfomances for all tests this user
        /// has taken.
        /// </summary>
        /// <returns>Overall perfomances</returns>
        public List<StudentTestPerfomanceOverviewModel> GetStudentTestPerfomances()
        {
            var requestUrl = $"{baseUrl}/user/student/testPerfomance";

            var token = Application.Current.Properties["token"] as string;
            if (!token.IsNullOrEmpty())
            {
                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", token);
            }

            var response = client.GetAsync(requestUrl).Result;
            var deserializedResponse = response.Content.ReadAsStringAsync().Result;

            var returnList = new List<StudentTestPerfomanceOverviewModel>();

            if (response.IsSuccessStatusCode)
            {
                var serializeOptions = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    WriteIndented = true
                };

                returnList = JsonSerializer.Deserialize<List<StudentTestPerfomanceOverviewModel>>(deserializedResponse, serializeOptions);
            }

            return returnList;
        }

        /// <summary>
        /// Get perfomance from one test, viewing result for each question in
        /// test.
        /// </summary>
        /// <param name="testId">Test</param>
        /// <returns>Perfomance details for one test</returns>
        public List<StudentTestPerfomanceDetailsModel> GetStudentTestPerfomanceDetails(int testId)
        {
            var requestUrl = $"{baseUrl}/user/student/testPerfomanceDetails?testId={testId}";

            var token = Application.Current.Properties["token"] as string;
            if (!token.IsNullOrEmpty())
            {
                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", token);
            }

            var response = client.GetAsync(requestUrl).Result;
            var deserializedResponse = response.Content.ReadAsStringAsync().Result;

            var returnList = new List<StudentTestPerfomanceDetailsModel>();

            if (response.IsSuccessStatusCode)
            {
                var serializeOptions = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    WriteIndented = true
                };

                returnList = JsonSerializer.Deserialize<List<StudentTestPerfomanceDetailsModel>>(deserializedResponse, serializeOptions);
            }

            return returnList;
        }
    }
}
