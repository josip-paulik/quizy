﻿using System;
using System.Collections.Generic;
using QuizyMobile.ViewModels;

namespace QuizyMobile.Services
{
    public interface IQuizyAPIService
    {
        /// <summary>
        /// Registers new user to application.
        /// </summary>
        /// <param name="registerModel">New user</param>
        /// <returns>Token model containing token.</returns>
        TokenModel Register(RegisterModel registerModel);

        /// <summary>
        /// Logs in existing user to application.
        /// </summary>
        /// <param name="loginModel">Existing user</param>
        /// <returns>Token model containing token.</returns>
        TokenModel Login(LoginModel loginModel);

        /// <summary>
        /// Tries to join test with provided access code.
        /// </summary>
        /// <param name="accessCode">Access code</param>
        /// <returns>True if user can join the test.</returns>
        bool CanJoin(string accessCode);

        /// <summary>
        /// Gets overall perfomances for all tests this user
        /// has taken.
        /// </summary>
        /// <returns>Overall perfomances</returns>
        List<StudentTestPerfomanceOverviewModel> GetStudentTestPerfomances();

        /// <summary>
        /// Get perfomance from one test, viewing result for each question in
        /// test.
        /// </summary>
        /// <param name="testId">Test</param>
        /// <returns>Perfomance details for one test</returns>
        List<StudentTestPerfomanceDetailsModel> GetStudentTestPerfomanceDetails(int testId);
    }
}
