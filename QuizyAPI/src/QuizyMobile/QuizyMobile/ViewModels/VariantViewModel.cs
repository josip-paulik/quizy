﻿using System;
using System.Collections.Generic;

namespace QuizyMobile.ViewModels
{
    public class VariantViewModel
    {
        public string Id { get; set; }
        public string QuestionText { get; set; }
        public List<string> OfferedAnswers { get; set; }
        public int NumberOfCorrectAnswers { get; set; }
    }
}
