﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QuizyMobile.ViewModels
{
    public class LoginModel
    {
        [Required(ErrorMessage = "emailAddress is required.")]
        [EmailAddress(ErrorMessage = "Email adress is in wrong format.")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "installationId is required.")]
        public string InstallationId { get; set; }
    }
}
