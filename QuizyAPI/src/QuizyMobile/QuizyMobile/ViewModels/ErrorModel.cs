﻿using System;
namespace QuizyMobile.ViewModels
{
    /// <summary>
    /// Model which will be returned if request to this app
    /// resulted in any kind of exception.
    /// </summary>
    public class ErrorModel
    {
        /// <summary>
        /// Error code from <see cref="ErrorCodesEnum">.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Error message for a certain error code.
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
