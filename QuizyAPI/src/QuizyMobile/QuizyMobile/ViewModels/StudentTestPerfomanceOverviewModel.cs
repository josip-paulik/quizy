﻿using System;
namespace QuizyMobile.ViewModels
{
    public class StudentTestPerfomanceOverviewModel
    {
        public int TestId { get; set; }
        public string QuizTitle { get; set; }
        public string QuizSummary { get; set; }
        public float PointsScored { get; set; }
        public float MaxPoints { get; set; }
        public DateTime DateTimeInitiated { get; set; }
    }
}
