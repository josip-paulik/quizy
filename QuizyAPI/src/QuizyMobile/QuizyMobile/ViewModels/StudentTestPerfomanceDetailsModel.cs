﻿using System;
using System.Collections.Generic;

namespace QuizyMobile.ViewModels
{
    public class StudentTestPerfomanceDetailsModel
    {
        public int QuestionIndex { get; set; }
        public string QuestionText { get; set; }
        public List<string> CorrectAnswers { get; set; }
        public List<string> GivenAnswers { get; set; }
        public float PointsScored { get; set; }
        public float MaximumPoints { get; set; }
        public List<string> OfferedAnswers { get; set; }
    }
}
