﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QuizyMobile.ViewModels
{
    public class RegisterModel
    {
        
        [EmailAddress(ErrorMessage = "Email adress is in wrong format.")]
        [Required(ErrorMessage = "Email address is required.")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Full name is required.")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Unique academic id is required.")]
        public string UniqueAcademicId { get; set; }

        [Required(ErrorMessage = "Installation id is required.")]
        public string InstallationId { get; set; }

        [Required(ErrorMessage = "Role is required")]
        public int Role { get; set; }   
    }
}
