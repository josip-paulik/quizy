﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuizyMobile.Services;
using QuizyMobile.Utility;
using Xamarin.Forms;

namespace QuizyMobile
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private readonly IQuizyAPIService quizyAPIService;

        public MainPage()
        {
            InitializeComponent();

            quizyAPIService = App.IoCContainer.GetInstance<IQuizyAPIService>();

            NavigationPage.SetHasBackButton(this, false);
            if (!Application.Current.Properties.ContainsKey("installationId"))
            {
                Navigation.PushAsync(new LoginPage());
            }
            else
            {
                var fullName = Application.Current.Properties["fullName"] as string;
                welcomeText.Text = $"Welcome {fullName}!";
            }
        }

        protected override void OnAppearing()
        {
            if (Application.Current.Properties.ContainsKey("token"))
            {
                FillTestPerfomanceScroll();
            }
        }

        private void FillTestPerfomanceScroll()
        {
            var testPerfomances = quizyAPIService.GetStudentTestPerfomances();


            var allPerfomancesStackLayout = new StackLayout();
            foreach (var testPerfomance in testPerfomances)
            {
                var perfomanceStackLayout = new StackLayout();
                var testIdHiddenField = new Label
                {
                    Text = testPerfomance.TestId.ToString(),
                    IsVisible = false
                };
                var quizTitleLabel = new Label
                {
                    Text = testPerfomance.QuizTitle,
                    FontAttributes = FontAttributes.Bold,
                    FontSize = Device.GetNamedSize(NamedSize.Header, typeof(Label))
                };
                var quizSummaryLabel = new Label
                {
                    Text = testPerfomance.QuizSummary,
                    FontSize = Device.GetNamedSize(NamedSize.Body, typeof(Label))
                };
                var pointScoredLabel = new Label
                {
                    Text = $"Result: {testPerfomance.PointsScored}/{testPerfomance.MaxPoints}",
                    TextColor = Color.Olive,
                    FontSize = Device.GetNamedSize(NamedSize.Body, typeof(Label))
                };
                var dateTimeLabel = new Label
                {
                    Text = testPerfomance.DateTimeInitiated.ToLocalTime().ToString("d.M.yyyy dddd H:mm"),
                };
                perfomanceStackLayout.Children.Add(testIdHiddenField);
                perfomanceStackLayout.Children.Add(quizTitleLabel);
                perfomanceStackLayout.Children.Add(quizSummaryLabel);
                perfomanceStackLayout.Children.Add(pointScoredLabel);
                perfomanceStackLayout.Children.Add(dateTimeLabel);

                var frame = new Frame
                {
                    Content = perfomanceStackLayout,
                    BorderColor = Color.Olive,
                };

                var tapGestureRecognizer = new TapGestureRecognizer
                {
                    Command = new Command(() => GetPerfomanceDetails(testPerfomance.TestId))
                };
                frame.GestureRecognizers.Add(tapGestureRecognizer);

                allPerfomancesStackLayout.Children.Add(frame);
            }
            testPerfomanceScroll.Content = allPerfomancesStackLayout;
        }

        private async void GetPerfomanceDetails(int testId)
        {
            await Navigation.PushAsync(new TestPerfomanceDetailsPage(testId));
        }
            
        async void joinTestButton_Clicked(System.Object sender, System.EventArgs e)
        {
            var accessCode = await DisplayPromptAsync("Join test", "Enter access code given by your lecturer");
            if (accessCode.IsNullOrEmpty())
            {
                return;
            }

            var canJoinTest = quizyAPIService.CanJoin(accessCode);
            if (!canJoinTest)
            {
                await DisplayAlert("Failed to join test", "Failed to join test with provided access code.", "OK");
                return;
            }

            await Navigation.PushAsync(new TestPage(accessCode));
        }
    }
}
