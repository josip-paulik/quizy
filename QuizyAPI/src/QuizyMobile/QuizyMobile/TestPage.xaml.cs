﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.AspNetCore.SignalR.Client;
using QuizyMobile.CustomComponents;
using QuizyMobile.Utility;
using QuizyMobile.Utility.Enums;
using QuizyMobile.ViewModels;
using Xamarin.Forms;

namespace QuizyMobile
{
    public partial class TestPage : ContentPage
    {
        private const int TimeToPrepareInSeconds = 3;
        private readonly HubConnection hubConnection;
        private VariantViewModel currentVariant;
        private QuestionTypeEnum currentQuestionType;
        public int SecondsLeft { get; set; }
        public bool AnswerToBeSubmitted { get; set; }

        private string accessCode;

        private string baseUrl = Device.RuntimePlatform == Device.Android ? "https://10.0.2.2:5001" : "https://localhost:5001";
        public TestPage(string accessCode)
        {
            InitializeComponent();

            BindingContext = this;

            NavigationPage.SetHasBackButton(this, false);
            
            testStateLayout.IsVisible = true;
            questionStateLayout.IsVisible = false;
            questionLayout.IsVisible = false;
            questionPrepareTimeLayout.IsVisible = false;

            hubConnection = new HubConnectionBuilder()
                .WithUrl($"{baseUrl}/testHub", options =>
                {
                    options.AccessTokenProvider = () => Task.FromResult(Application.Current.Properties["token"] as string);
                    options.HttpMessageHandlerFactory = (message) =>
                    {
                        if (message is HttpClientHandler clientHandler)
                            // bypass SSL certificate
                            clientHandler.ServerCertificateCustomValidationCallback +=
                                (sender, certificate, chain, sslPolicyErrors) => { return true; };
                        return message;
                    };
                })
                .WithAutomaticReconnect()
                .Build();

            this.accessCode = accessCode;
        }

        protected override async void OnAppearing()
        {

            ConfigureHubConnection();

            await hubConnection.StartAsync();

            await hubConnection.InvokeAsync("JoinTest", accessCode);

            base.OnAppearing();
        }

        protected override async void OnDisappearing()
        {
            if (hubConnection.State != HubConnectionState.Disconnected)
            {
                await hubConnection.InvokeAsync("ExitTest", accessCode);
                await hubConnection.StopAsync();
            }
            
            base.OnDisappearing();
        }
 
        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                var exitTest = await DisplayAlert(
                    "Exit test?",
                    "If you exit the test you will no longer have ability to enter test again. Your submitted answers are saved already.",
                    "Yes",
                    "No");
                if (exitTest)
                {
                    await this.Navigation.PopAsync();
                }
            });

            return true;
        }

        private void ConfigureHubConnection()
        {
            hubConnection.On<int, int, int, VariantViewModel>("PrepareNextQuestion", (questionIndex, timeToAnswerInSeconds, questionType, variant) =>
            {
                testStateLayout.IsVisible = false;

                questionText.Text = $"{questionIndex}. {variant.QuestionText}";
                numberOfCorrectAnswersLabel.Text = $"There are {variant.NumberOfCorrectAnswers} correct answers.";
                currentVariant = variant;
                currentQuestionType = (QuestionTypeEnum)questionType;

                answerInputLayout.Children.Clear();
                if (questionType == (int)QuestionTypeEnum.Choice)
                {
                    foreach (var offeredAnswer in variant.OfferedAnswers)
                    {
                        answerInputLayout.Children.Add(new ToggleButton
                        {
                            Text = offeredAnswer
                        });
                    }
                }
                else
                {
                    answerInputLayout.Children.Add(new Entry
                    {
                        Placeholder = "Enter your answer here"
                    });
                }

                // prepare preparation timer
                SecondsLeft = TimeToPrepareInSeconds;

                questionStateLayout.IsVisible = false;
                questionPrepareTimeLayout.IsVisible = true;

                OnPropertyChanged(nameof(SecondsLeft));
                Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                {
                    if (SecondsLeft > 0)
                    {
                        SecondsLeft--;
                        OnPropertyChanged(nameof(SecondsLeft));
                        return true;
                    }

                    questionLayout.IsVisible = true;
                    SecondsLeft = timeToAnswerInSeconds;
                    OnPropertyChanged(nameof(SecondsLeft));
                    AnswerToBeSubmitted = true;
                    Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                    {
                        if (SecondsLeft > 0 && AnswerToBeSubmitted)
                        {
                            SecondsLeft--;
                            OnPropertyChanged(nameof(SecondsLeft));
                            return true;
                        }

                        if (!AnswerToBeSubmitted)
                        {
                            return false;
                        }

                        submitButton_Clicked(null, null);

                        return false;
                    });
                    return false;
                });

            });

            hubConnection.On<int>("TestLobbyUpdated", studentCount =>
            {
                testStateLabel.Text = $"Number of students in lobby: {studentCount}";             
            });

            hubConnection.On<int, int>("AnswerSubmitted", (numberOfStudentsSubmittedAnswer, numberOfStudentsJoinedLobby) =>
            {
                questionStateLabel.Text = $"Answers submitted {numberOfStudentsSubmittedAnswer}/{numberOfStudentsJoinedLobby}";
            });

            hubConnection.On("TestEnded",() =>
            {
                OnLeaving();
            });
        }

        async void submitButton_Clicked(System.Object sender, System.EventArgs e)
        {
            var submittedAnswers = new List<string>();
            if (currentQuestionType == QuestionTypeEnum.Choice)
            {
                foreach (ToggleButton button in answerInputLayout.Children)
                {
                    if (button.IsSelected)
                    {
                        submittedAnswers.Add(button.Text);
                    }
                }
            }
            else
            {
                Entry answerEntry = answerInputLayout.Children[0] as Entry;

                if (!answerEntry.Text.IsNullOrEmpty())
                {
                    submittedAnswers.Add(answerEntry.Text);
                }
            }

            questionLayout.IsVisible = false;
            questionPrepareTimeLayout.IsVisible = false;
            questionStateLayout.IsVisible = true;

            if (hubConnection.State != HubConnectionState.Disconnected)
            {
                await hubConnection.InvokeAsync(
                   "SubmitAnswer",
                   accessCode,
                   currentVariant.Id,
                   submittedAnswers.ToArray());
            }

            AnswerToBeSubmitted = false;
        }
        
        private async void OnLeaving()
        {
            await DisplayAlert("Test ended", "Test Ended", "OK");
            if (hubConnection.State != HubConnectionState.Disconnected)
            {
                await hubConnection.StopAsync();
            }
            await Navigation.PopAsync();
        }
    }
}
