﻿using System;
using QuizyMobile.Services;
using QuizyMobile.ViewModels;
using SimpleInjector;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QuizyMobile
{
    public partial class App : Application
    {
        private static Container ioCContainer = new SimpleInjector.Container();
        private readonly IQuizyAPIService quizyAPIService;

        public static Container IoCContainer
        {
            get => ioCContainer;
            set => ioCContainer = value;
        }

        public App()
        {
            InitializeComponent();

            ConfigureDependencies();
            quizyAPIService = IoCContainer.GetInstance<IQuizyAPIService>();
            MainPage = new NavigationPage(new MainPage());

            
        }

        private void ConfigureDependencies()
        {
            IoCContainer.Register<IQuizyAPIService, QuizyAPIService>(Lifestyle.Transient);
        }

        protected override void OnStart()
        {
            if (Application.Current.Properties.ContainsKey("emailAddress"))
            {
                var token = quizyAPIService.Login(new LoginModel
                {
                    EmailAddress = Application.Current.Properties["emailAddress"] as string,
                    InstallationId = Application.Current.Properties["installationId"] as string
                });
                Application.Current.Properties["token"] = token.Token;
            }
        }

        protected override async void OnSleep()
        {
            await Application.Current.SavePropertiesAsync();
        }

        protected override async void OnResume()
        {
            await Application.Current.SavePropertiesAsync();
        }
    }

    
}
