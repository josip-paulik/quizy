﻿using System;
using System.Collections.Generic;
using QuizyAPI.Controllers.ViewModels;

namespace QuizyAPI.Services
{
    public interface IStudentQuestionPerfomanceService
    {
        /// <summary>
        /// Gets student perfomance overview for all tests that
        /// the student has taken.
        /// </summary>
        /// <param name="studentId">Student identifier</param>
        /// <returns>Summary data for all tests that he has taken.</returns>
        List<StudentTestPerfomanceOverviewModel> GetStudentPerfomanceOverview(int studentId);


        /// <summary>
        /// Gets perfomance data for one test.
        /// Such as what did he answer on a particuar question,
        /// how many points question is worth etc.
        /// <see cref="StudentTestPerfomanceDetailsModel"/>.
        /// </summary>
        /// <param name="studentId">Student identifier</param>
        /// <param name="testId">Test identifier</param>
        /// <returns>Perfomance data for one test.</returns>
        List<StudentTestPerfomanceDetailsModel> GetStudentPerfomanceDetails(int studentId, int testId);
    }
}
