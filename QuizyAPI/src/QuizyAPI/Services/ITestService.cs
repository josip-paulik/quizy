﻿using System;
using System.Collections.Generic;
using QuizyAPI.Controllers.ViewModels;
using QuizyAPI.Utility;

namespace QuizyAPI.Services
{
    public interface ITestService
    {
        /// <summary>
        /// Starts test lobby and insetrs new test into database.
        /// </summary>
        /// <param name="professorId">Professor who started the test.</param>
        /// <param name="quizId">Quiz with which test started.</param>
        /// <param name="groupId">Group that is taking the test.</param>
        /// <returns>Data required to start the test.</returns>
        TestOverviewModel StartTestLobby(int professorId, int? quizId, int groupId);

        /// <summary>
        /// Starts test and dissalows further entry.
        /// </summary>
        /// <param name="professorId">Professor who starts the test</param>
        /// <param name="accessCode">Test</param>
        /// <exception cref="TestNotFoundException" />
        /// <exception cref="ProfessorDoesNotHaveRightsToStartThisTestException" />
        void StartTest(int professorId, string accessCode);

        /// <summary>
        /// Tries to find the test in cache which has matching access code and also
        /// a test that user can join.
        /// </summary>
        /// <param name="accessCode">Access code</param>
        /// <param name="studentId">Student who is trying to join.</param>
        /// <returns>True if student can join the test, false otherwise</returns>
        bool CanJoinTest(int studentId, string accessCode);

        /// <summary>
        /// Gets attendances for group.
        /// </summary>
        /// <param name="groupId">Group</param>
        /// <returns>List of attendance checks(with students which
        /// participated in attendance check) with datetimes when attendance checks were performed.</returns>
        List<AttendanceModel> GetAttendancesForGroup(int groupId);
    }
}
