﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuizyAPI.Controllers.ViewModels;
using QuizyAPI.Repositories;
using QuizyAPI.Utility;
using QuizyAPI.Utility.Enums;

namespace QuizyAPI.Services
{
    public class StudentQuestionPerfomanceService : IStudentQuestionPerfomanceService
    {
        private readonly IStudentQuestionPerfomanceRepository perfomanceRepository;
        public StudentQuestionPerfomanceService(
            IStudentQuestionPerfomanceRepository perfomanceRepository)
        {
            this.perfomanceRepository = perfomanceRepository;
        }

        /// <summary>
        /// Gets student perfomance overview for all tests that
        /// the student has taken.
        /// </summary>
        /// <param name="studentId">Student identifier</param>
        /// <returns>Summary data for all tests that he has taken.</returns>
        public List<StudentTestPerfomanceOverviewModel> GetStudentPerfomanceOverview(int studentId)
        {
            var testPerfomances = perfomanceRepository.GetStudentPerfomances(studentId);

            var testPerfomancesOverviewGrouped = testPerfomances
                .GroupBy(x => x.TestId);

            var returnList = new List<StudentTestPerfomanceOverviewModel>();

            foreach (var testPerfomanceGroup in testPerfomancesOverviewGrouped)
            {
                var firstTestFromGroup = testPerfomanceGroup.First();

                var testQuiz = firstTestFromGroup.Test.Quiz;
                var maximumPossiblePointsOnTest = testQuiz.Questions.Sum(x => x.MaximumPoints);

                var dateTimeInitiated = firstTestFromGroup.Test.DateTimeInitiated;

                var accumulatedPointsOnTest = 0f;

                foreach (var testPerfomance in testPerfomanceGroup)
                {
                    var maximumPossiblePointsOnQuestion = testPerfomance.Variant.Question.MaximumPoints;

                    var correctAnswers = testPerfomance.Variant.Answers
                        .Where(x => x.IsCorrect)
                        .Select(x => x.Text)
                        .ToList();
                    var givenAnswers = testPerfomance.GivenAnswers
                        .Select(x => x.GivenAnswerText)
                        .ToList();

                    if (AreAllAnswersCorrect(givenAnswers, correctAnswers))
                    {
                        accumulatedPointsOnTest += maximumPossiblePointsOnQuestion;
                    }
                }

                var testPerfomanceOverviewModel = new StudentTestPerfomanceOverviewModel
                {
                    TestId = testPerfomanceGroup.Key,
                    QuizTitle = testQuiz.Title,
                    QuizSummary = testQuiz.Summary,
                    PointsScored = accumulatedPointsOnTest,
                    MaxPoints = maximumPossiblePointsOnTest,
                    DateTimeInitiated = dateTimeInitiated
                };

                returnList.Add(testPerfomanceOverviewModel);
            }

            returnList = returnList
                .OrderByDescending(x => x.DateTimeInitiated)
                .ToList();

            return returnList;
        }

        /// <summary>
        /// Gets perfomance data for one test.
        /// Such as what did he answer on a particuar question,
        /// how many points question is worth etc.
        /// <see cref="StudentTestPerfomanceDetailsModel"/>.
        /// </summary>
        /// <param name="studentId">Student identifier</param>
        /// <param name="testId">Test identifier</param>
        /// <returns>Perfomance data for one test.</returns>
        public List<StudentTestPerfomanceDetailsModel> GetStudentPerfomanceDetails(int studentId, int testId)
        {
            var testPerfomances = perfomanceRepository.GetStudentPerfomanceDetails(studentId, testId);

            if (testPerfomances.IsNullOrEmpty())
            {
                return new List<StudentTestPerfomanceDetailsModel>();
            }

            var returnList = testPerfomances
                .Select(x =>
                {
                    var correctAnswers = x.Variant.Answers
                        .Where(x => x.IsCorrect)
                        .Select(x => x.Text)
                        .ToList();
                    var givenAnswers = x.GivenAnswers
                        .Select(x => x.GivenAnswerText)
                        .ToList();
                    var offeredAnswers = x.Variant.Question.QuestionTypeId == (int)QuestionTypeEnum.Choice
                        ? x.Variant.Answers.Select(x => x.Text).ToList() : new List<string>();
                    return new StudentTestPerfomanceDetailsModel
                    {
                        QuestionIndex = x.Variant.Question.Index,
                        QuestionText = x.Variant.QuestionText,
                        MaximumPoints = x.Variant.Question.MaximumPoints,
                        CorrectAnswers = correctAnswers,
                        GivenAnswers = givenAnswers,
                        OfferedAnswers = offeredAnswers,
                        PointsScored = AreAllAnswersCorrect(givenAnswers, correctAnswers) ? x.Variant.Question.MaximumPoints : 0
                    };
                })
                .OrderBy(x => x.QuestionIndex)
                .ToList();

            return returnList;
        }

        /// <summary>
        /// Compares list of given and correct answers
        /// and determines if all given answers are correct.
        /// </summary>
        /// <param name="givenAnswers">Given answers</param>
        /// <param name="correctAnswers">Correct answers</param>
        /// <returns>True if all answers are correct, false otherwise</returns>
        private bool AreAllAnswersCorrect(List<string> givenAnswers, List<string> correctAnswers)
        {
            if (givenAnswers.IsNullOrEmpty())
            {
                return false;
            }

            foreach (var givenAnswer in givenAnswers)
            {
                if (!correctAnswers.Contains(givenAnswer))
                {
                    return false;
                }
            }

            return true;
        }

    }
}
