﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using QuizyAPI.Controllers.ViewModels;
using QuizyAPI.DAL.Models;
using QuizyAPI.Repositories;
using QuizyAPI.Utility;

namespace QuizyAPI.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly IConfiguration configuration;

        public UserService(IUserRepository userRepository, IConfiguration configuration)
        {
            this.userRepository = userRepository;
            this.configuration = configuration;
        }

        /// <summary>
        /// Get token for authentication in app.
        /// </summary>
        /// <param name="user">User trying to log in</param>
        /// <returns>Token user is going to use for authorization.</returns>
        public string GetToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration["Jwt:Key"]));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.RoleId.ToString())
                }),
                Expires = DateTime.UtcNow.AddHours(10),
                SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature),
                Audience = configuration["Jwt:Audience"],
                IssuedAt = DateTime.UtcNow,
                Issuer = configuration["Jwt:Issuer"]
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }

        /// <summary>
        /// Registers new user.
        /// </summary>
        /// <param name="registerModel">User that is going to be registered.</param>
        /// <returns>User that is registered.</returns>
        public User Register(RegisterModel registerModel)
        {
            var usersWithSameMail = userRepository
                .Filter(
                x => x.EmailAddress == registerModel.EmailAddress || x.UniqueAcademicId == registerModel.UniqueAcademicId,
                x => x.EmailAddress);
            
            if (!usersWithSameMail.IsNullOrEmpty())
            {
                return null;
            }

            var newUser = new User
            {
                EmailAddress = registerModel.EmailAddress,
                FullName = registerModel.FullName,
                InstallationId = PasswordHashing.Hash(registerModel.InstallationId),
                RoleId = registerModel.Role,
                UniqueAcademicId = registerModel.UniqueAcademicId
            };

            userRepository.Add(newUser);

            return newUser;
        }

        /// <summary>
        /// Logins existing user.
        /// </summary>
        /// <param name="loginModel">User that wants to login.</param>
        /// <returns>Returns user if it exists.</returns>
        public User TryLogin(LoginModel loginModel)
        {
            var usersWithSameMail = userRepository
                .Filter(
                x => x.EmailAddress == loginModel.EmailAddress,
                x => x);
            if (usersWithSameMail.IsNullOrEmpty())
            {
                return null;
            }

            var existingUser = usersWithSameMail.First();

            var passwordsMatch = PasswordHashing.Check(existingUser.InstallationId, loginModel.InstallationId);

            if (!passwordsMatch)
            {
                return null;
            }

            return existingUser;
        }
    }
}
