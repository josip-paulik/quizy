﻿using System;
using QuizyAPI.Controllers.ViewModels;
using QuizyAPI.DAL.Models;

namespace QuizyAPI.Services
{
    public interface IUserService
    {
        /// <summary>
        /// Registers new user.
        /// </summary>
        /// <param name="registerModel">User that is going to be registered.</param>
        /// <returns>User that is registered or null if user could not be registered.</returns>
        User Register(RegisterModel registerModel);

        /// <summary>
        /// Logins existing user.
        /// </summary>
        /// <param name="loginModel">User that wants to login.</param>
        /// <returns>Returns user if it exists.</returns>
        User TryLogin(LoginModel loginModel);

        /// <summary>
        /// Get token for authentication in app.
        /// </summary>
        /// <param name="user">User trying to log in</param>
        /// <returns>Token user is going to use for authorization.</returns>
        string GetToken(User user);
    }
}
