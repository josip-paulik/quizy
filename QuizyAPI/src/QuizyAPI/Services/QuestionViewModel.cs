﻿using System.Collections.Generic;

namespace QuizyAPI.Services
{
    /// <summary>
    /// This model is used to store questions
    /// in cache for faster retrieval.
    /// </summary>
    public class QuestionViewModel
    {
        /// <summary>
        /// Order of question
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Type of question
        /// </summary>
        public int QuestionType { get; set; }

        /// <summary>
        /// Variants for this question
        /// </summary>
        public List<VariantViewModel> Variants { get; set; }

        /// <summary>
        /// How much time does student have to answer this question.
        /// </summary>
        public int TimeToAnswerInSeconds { get; set; }
    }
}