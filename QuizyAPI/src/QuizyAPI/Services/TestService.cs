﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text.Json;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Distributed;
using QuizyAPI.Controllers.ViewModels;
using QuizyAPI.DAL.Models;
using QuizyAPI.Hubs;
using QuizyAPI.Repositories;
using QuizyAPI.Utility;
using QuizyAPI.Utility.Enums;

namespace QuizyAPI.Services
{
    public class TestService : ITestService
    {
        private readonly ITestRepository testRepository;
        private readonly IQuizRepository quizRepository;
        private readonly IDistributedCache distributedCache;
        private readonly IGenericRepository<StudentGroup> studentGroupRepository;
        private readonly IStudentQuestionPerfomanceRepository perfomanceRepository;
        private readonly IHubContext<TestHub> hubContext;
        private readonly IGenericRepository<QuizCreator> quizCreatorRepository;
        private readonly IGenericRepository<ProfessorGroup> professorGroupsRepository;
        private readonly IGenericRepository<Group> groupRepository;
        

        public TestService(
            ITestRepository testRepository,
            IDistributedCache distributedCache,
            IGenericRepository<StudentGroup> studentGroupRepository,
            IQuizRepository quizRepository,
            IStudentQuestionPerfomanceRepository perfomanceRepository,
            IHubContext<TestHub> hubContext,
            IGenericRepository<QuizCreator> quizCreatorRepository,
            IGenericRepository<Group> groupRepository,
            IGenericRepository<ProfessorGroup> professorGroupsRepository)
        {
            this.testRepository = testRepository;
            this.distributedCache = distributedCache;
            this.studentGroupRepository = studentGroupRepository;
            this.quizRepository = quizRepository;
            this.hubContext = hubContext;
            this.perfomanceRepository = perfomanceRepository;
            this.quizCreatorRepository = quizCreatorRepository;
            this.groupRepository = groupRepository;
            this.professorGroupsRepository = professorGroupsRepository;
        }

        /// <summary>
        /// Starts test lobby and insetrs new test into database.
        /// </summary>
        /// <param name="professorId">Professor who started the test.</param>
        /// <param name="quizId">Quiz with which test started.</param>
        /// <param name="groupId">Group that is taking the test.</param>
        /// <returns>Data required to start the test.</returns>
        public TestOverviewModel StartTestLobby(int professorId, int? quizId, int groupId)
        {   
            if (quizId.HasValue)
            {
                var quiz = quizRepository
                    .Filter(
                        x => x.Id == quizId.Value,
                        x => x)
                    .FirstOrDefault();
                if (quiz == null)
                {
                    throw new QuizNotFoundException();
                }

                var quizCreator = quizCreatorRepository
                    .Filter(
                        x => x.ProfessorId == professorId && x.QuizId == quizId,
                        x => x)
                    .FirstOrDefault();
                if (quizCreator == null)
                {
                    throw new ProfessorIsNotCreatorOfThisQuizException();
                }
            }

            var group = groupRepository
                .Filter(
                    x => x.Id == groupId,
                    x => x)
                .FirstOrDefault();
            if (group == null)
            {
                throw new GroupNotFoundException();
            }

            var professorGroup = professorGroupsRepository
                .Filter(
                    x => x.GroupId == groupId && x.ProfessorId == professorId,
                    x => x)
                .FirstOrDefault();
            if (professorGroup == null)
            {
                throw new ProfessorIsNotLeadingGroupException();
            }

            var groupsTakingTestKey = "GroupsTakingTest";

            var groupsTakingTestList = distributedCache.GetObjectFromCache<List<int>>(groupsTakingTestKey);
            if (!groupsTakingTestList.IsNullOrEmpty())
            {
                if (groupsTakingTestList.Contains(groupId))
                {
                    throw new GroupAlreadyHasExamsException();
                }

                groupsTakingTestList.Add(groupId);
            }
            else
            {
                groupsTakingTestList = new List<int> { groupId };
            }


            distributedCache.SetObjectInCache(groupsTakingTestKey, groupsTakingTestList);
            
            var newTest = new Test
            {
                ProfessorId = professorId,
                GroupId = groupId,
                QuizId = quizId,
                DateTimeInitiated = DateTime.UtcNow
            };

            testRepository.Add(newTest);
            var testAccessCodeValue = CreateRandomAccessCode();

            while (!distributedCache.GetString(testAccessCodeValue).IsNullOrEmpty())
            {
                testAccessCodeValue = CreateRandomAccessCode();
            }

            var questions = GetQuestionsForCache(quizId);

            var testState = new CurrentTestState
            {
                TestId = newTest.Id,
                GroupId = groupId,
                ProfessorId = professorId,
                Questions = questions,
                EntryAllowed = true
            };

            distributedCache.SetObjectInCache(testAccessCodeValue, testState);

            var testOverviewModel = new TestOverviewModel
            {
                AccessCode = testAccessCodeValue,
                TestId = newTest.Id
            };

            return testOverviewModel;
        }

        

        /// <summary>
        /// Tries to find the test in cache which has matching access code and also
        /// a test that user can join.
        /// </summary>
        /// <param name="accessCode">Access code</param>
        /// <param name="studentId">Student who is trying to join.</param>
        /// <returns>True if student can join the test, false otherwise</returns>
        public bool CanJoinTest(int studentId, string accessCode)
        {
            var testState = distributedCache.GetObjectFromCache<CurrentTestState>(accessCode);

            if (testState == null)
            {
                throw new TestNotFoundException();
            }
            if (testState.StudentsJoined.Contains(studentId))
            {
                throw new StudentIsInTestAlreadyException();
            }

            var testGroupId = testState.GroupId;
            var studentGroup = studentGroupRepository
                .Filter(
                    x => x.GroupId == testGroupId && x.StudentId == studentId,
                    x => x
                );

            if (studentGroup.IsNullOrEmpty())
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Starts test and dissalows further entry.
        /// </summary>
        /// <param name="professorId">Professor who starts the test</param>
        /// <param name="accessCode">Test</param>
        /// <exception cref="TestNotFoundException" />
        /// <exception cref="ProfessorDoesNotHaveRightsToStartThisTestException" />
        public async void StartTest(int professorId, string accessCode)
        {
            var currentTestState = distributedCache.GetObjectFromCache<CurrentTestState>(accessCode);
            if (currentTestState == null)
            {
                throw new TestNotFoundException();
            }
            if (currentTestState.ProfessorId != professorId)
            {
                throw new ProfessorDoesNotHaveRightsToStartThisTestException();
            }
            if (!currentTestState.EntryAllowed)
            {
                throw new TestHasAlreadyStartedException();
            }

            currentTestState.EntryAllowed = false;
            currentTestState.CurrentQuestionIndex = 1;
            distributedCache.SetObjectInCache(accessCode, currentTestState);

            
            if (currentTestState.CurrentQuestionIndex > currentTestState.Questions.Count())
            {
                // the test is only an attendance check
                await hubContext.Clients.Group(accessCode).SendAsync("TestEnded");

                foreach (var connectionId in currentTestState.ConnectionIds)
                {
                    await hubContext.Groups.RemoveFromGroupAsync(connectionId, accessCode);
                }

                var groupsTakingTestKey = "GroupsTakingTest";
                var groupsTakingTestList = distributedCache.GetObjectFromCache<List<int>>(groupsTakingTestKey);

                foreach (var studentId in currentTestState.StudentsJoined)
                {
                    var studentQuestionPerfomance = new StudentQuestionPerfomance
                    {
                        Id = Guid.NewGuid().ToString(),
                        StudentId = studentId,
                        TestId = currentTestState.TestId
                    };

                    perfomanceRepository.Add(studentQuestionPerfomance);
                }

                if (groupsTakingTestList.Contains(currentTestState.GroupId))
                {
                    groupsTakingTestList.Remove(currentTestState.GroupId);
                    distributedCache.SetObjectInCache(groupsTakingTestKey, groupsTakingTestList);
                }

                distributedCache.Remove(accessCode);
            }
            else
            {
                var currentQuestion = currentTestState.GetCurrentQuestion();

                foreach (var connectionId in currentTestState.ConnectionIds)
                {
                    var chosenVariant = currentTestState.GetRandomVariantForCurrentQuestion();

                    await hubContext.Clients.Client(connectionId)
                        .SendAsync("PrepareNextQuestion",
                            currentTestState.CurrentQuestionIndex,
                            currentQuestion.TimeToAnswerInSeconds,
                            currentQuestion.QuestionType,
                            chosenVariant);
                }
            }
        }

        /// <summary>
        /// Gets attendances for group.
        /// </summary>
        /// <param name="groupId">Group</param>
        /// <returns>List of attendance checks(with students which
        /// participated in attendance check) with datetimes when attendance checks were performed.</returns>
        public List<AttendanceModel> GetAttendancesForGroup(int groupId)
        {
            var attendanceChecks = testRepository.GetAttendanceChecksForGroup(groupId);

            var returnList = attendanceChecks
                .Select(x => new AttendanceModel
                {
                    DateTimeInitiated = x.DateTimeInitiated,
                    FullNames = x.StudentQuestionPerfomances
                        .Select(x => x.Student.FullName)
                        .ToList()
                })
                .ToList();

            return returnList;
        }

        /// <summary>
        /// Gets questions from quiz in form of a model
        /// which is going to be  stored on cache.
        /// </summary>
        /// <param name="quizId">Quiz identifier where questions are from</param>
        /// <returns>List of questions for cache</returns>
        private List<QuestionViewModel> GetQuestionsForCache(int? quizId)
        {
            if (!quizId.HasValue || quizId == 0)
            {
                return new List<QuestionViewModel>();
            }
            var quiz = quizRepository.GetQuizDetails(quizId.Value);

            var questions = quiz.Questions
                .Select(x => new QuestionViewModel
                {
                    Index = x.Index,
                    QuestionType = x.QuestionTypeId,
                    TimeToAnswerInSeconds = x.TimeToAnswerInSeconds,
                    Variants = x.Variants
                       .Select(y => new VariantViewModel
                       {
                           OfferedAnswers = x.QuestionTypeId == (int)QuestionTypeEnum.Choice ?
                                y.Answers.Select(z => z.Text).ToList() : new List<string>(),
                           NumberOfCorrectAnswers = x.QuestionTypeId == (int)QuestionTypeEnum.Choice ?
                                y.Answers.Where(x => x.IsCorrect).Count() : 1,
                           QuestionText = y.QuestionText,
                           Id = y.Id
                       })
                       .ToList()

                })
                .OrderBy(x => x.Index)
                .ToList();

            return questions;
        }

        /// <summary>
        /// Creates random access code consisting of
        /// 6 alphanumeric symbols.
        /// </summary>
        /// <returns>Created random access code.</returns>
        private string CreateRandomAccessCode()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            var accessCodeLength = 6;
            var newAccessCodeArray = new char[accessCodeLength];

            var random = new Random();

            for (int i = 0; i < accessCodeLength; i++)
            {
                newAccessCodeArray[i] = chars[random.Next(0, chars.Length)];
            }

            var newAccessCode = string.Join(string.Empty, newAccessCodeArray);

            return newAccessCode;
        }
    }
}
