﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QuizyAPI.Services
{
    public class CurrentTestState
    {
        /// <summary>
        /// Test identifier
        /// </summary>
        public int TestId { get; set; }

        /// <summary>
        /// Group idenetifier for group that takes the test
        /// </summary>
        public int GroupId { get; set; }

        /// <summary>
        /// Professor that started the test
        /// </summary>
        public int ProfessorId { get; set; }

        /// <summary>
        /// Students that have joined this test
        /// </summary>
        public List<int> StudentsJoined { get; set; }

        /// <summary>
        /// List of questions that will be on this test.
        /// </summary>
        public List<QuestionViewModel> Questions { get; set; }

        /// <summary>
        /// Is entry allowed
        /// </summary>
        public bool EntryAllowed { get; set; }

        /// <summary>
        /// Current question that the test is on.
        /// </summary>
        public int CurrentQuestionIndex { get; set; }

        /// <summary>
        /// Which students have submitted answer on current question.
        /// </summary>
        public List<int> StudentSubmittedAnswerOnCurrentQuestion { get; set; }

        /// <summary>
        /// Connection ids for SignalR connection. Used for sending random variants
        /// and cleaning up process.
        /// </summary>
        public List<string> ConnectionIds { get; set; }

        /// <summary>
        /// Gets random variant for current question.
        /// </summary>
        /// <returns>Random variant</returns>
        public VariantViewModel GetRandomVariantForCurrentQuestion()
        {
            var question = GetCurrentQuestion();
            var randomInt = new Random().Next(0, question.Variants.Count());

            var randomVariant = question.Variants[randomInt];
            return randomVariant;
        }

        /// <summary>
        /// Gets current question
        /// </summary>
        /// <returns>Current question</returns>
        public QuestionViewModel GetCurrentQuestion()
        {
            var question = Questions[CurrentQuestionIndex - 1];
            return question;
        }

        public CurrentTestState()
        {
            StudentsJoined = new List<int>();
            Questions = new List<QuestionViewModel>();
            StudentSubmittedAnswerOnCurrentQuestion = new List<int>();
            ConnectionIds = new List<string>();
        }
    }
}
