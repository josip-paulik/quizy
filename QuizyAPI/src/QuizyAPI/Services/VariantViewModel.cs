﻿using System.Collections.Generic;

namespace QuizyAPI.Services
{
    /// <summary>
    /// This model is used to store variants
    /// in cache for faster retrieval.
    /// </summary>
    public class VariantViewModel
    {
        /// <summary>
        /// Variant identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Text of the question.
        /// </summary>
        public string QuestionText { get; set; }

        /// <summary>
        /// Offered answers if there are any(choice or open text).
        /// </summary>
        public List<string> OfferedAnswers { get; set; }

        /// <summary>
        /// How many correct answers are there.
        /// </summary>
        public int NumberOfCorrectAnswers { get; set; }
    }
}