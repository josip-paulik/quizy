﻿using System;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using QuizyAPI.DAL.Models;

namespace QuizyAPI.DAL
{
    public class ApplicationContext : DbContext
    {

        public ApplicationContext(DbContextOptions<ApplicationContext> dbContextOptions) : base(dbContextOptions)
        {
            ChangeTracker.LazyLoadingEnabled = false;
        }

        public DbSet<Answer> Answers { get; set; }
        public DbSet<GivenAnswer> GivenAnswers { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<ProfessorGroup> ProfessorGroups { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Quiz> Quizzes { get; set; }
        public DbSet<QuizCreator> QuizCreators { get; set; }
        public DbSet<StudentGroup> StudentGroups { get; set; }
        public DbSet<StudentQuestionPerfomance> StudentQuestionPerfomances { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Variant> Variants { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
