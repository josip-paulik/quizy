﻿using System;
using System.Collections.Generic;

namespace QuizyAPI.DAL.Models
{
    public class StudentQuestionPerfomance
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Answers that are given to answer
        /// </summary>
        public List<GivenAnswer> GivenAnswers { get; set; }

        /// <summary>
        /// Id of the student for which perfomance is recorded.
        /// </summary>
        public int? StudentId { get; set; }

        /// <summary>
        /// Student for which the perfomance is recorded.
        /// </summary>
        public User Student { get; set; }

        /// <summary>
        /// Id of the variant that this student got at the time of solving the quiz.
        /// </summary>
        public string VariantId { get; set; }

        /// <summary>
        /// Variant that the student got at the time of solving the quiz.
        /// </summary>
        public Variant Variant { get; set; }

        /// <summary>
        /// Id of the test that was solved
        /// </summary>
        public int TestId { get; set; }

        /// <summary>
        /// Test that was solved
        /// </summary>
        public Test Test { get; set; }

        public StudentQuestionPerfomance()
        {
            GivenAnswers = new List<GivenAnswer>();
        }
    }
}
