﻿using System;
namespace QuizyAPI.DAL.Models
{
    public class GivenAnswer
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Binds to student question perfomance.
        /// This is its identifier.
        /// </summary>
        public string StudentQuestionPerfomanceId { get; set; }

        /// <summary>
        /// Binds to studnet question perfomance.
        /// </summary>
        public StudentQuestionPerfomance StudentQuestionPerfomance { get; set; }

        /// <summary>
        /// Given answer by student.
        /// </summary>
        public string GivenAnswerText { get; set; }
    }
}
