﻿using System;
using System.Collections.Generic;

namespace QuizyAPI.DAL.Models
{
    public class User
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// User's full name
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Unique academic identifier for academic citizen
        /// </summary>
        public string UniqueAcademicId { get; set; }

        /// <summary>
        /// App installation identifier, encrypted.
        /// </summary>
        public string InstallationId { get; set; }

        /// <summary>
        /// Email adress
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Identifier of user's role.
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        /// All quizes user has created.
        /// </summary>
        public List<QuizCreator> QuizzesCreated { get; set; }

        /// <summary>
        /// All tests that professor has initated.
        /// </summary>
        public List<Test> TestsInitated { get; set; }

        /// <summary>
        /// User is in these groups as student.
        /// </summary>
        public List<StudentGroup> GroupsJoined { get; set; }

        /// <summary>
        /// Grous which user leads(professor).
        /// </summary>
        public List<ProfessorGroup> GroupsLeading { get; set; }

        /// <summary>
        /// All question student has answered to.
        /// </summary>
        public List<StudentQuestionPerfomance> StudentQuestionPerfomances { get; set; }

        public User()
        {
            QuizzesCreated = new List<QuizCreator>();
            TestsInitated = new List<Test>();
            GroupsJoined = new List<StudentGroup>();
            StudentQuestionPerfomances = new List<StudentQuestionPerfomance>();
        }
    }
}
