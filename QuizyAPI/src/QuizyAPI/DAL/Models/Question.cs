﻿using System;
using System.Collections.Generic;

namespace QuizyAPI.DAL.Models
{
    public class Question
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// This number determines in which order in quiz
        /// does this question appear.
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// How much points is possible to achieve
        /// on this question.
        /// </summary>
        public float MaximumPoints { get; set; }

        /// <summary>
        /// How much time inseconds to answer this question.
        /// </summary>
        public int TimeToAnswerInSeconds { get; set; }

        /// <summary>
        /// Id of the quiz this question is in.
        /// </summary>
        public int QuizId { get; set; }

        /// <summary>
        /// Quiz the question is in.
        /// </summary>
        public Quiz Quiz { get; set; }

        /// <summary>
        /// Variants that this question can have.
        /// </summary>
        public List<Variant> Variants { get; set; }

        /// <summary>
        /// Id of the question type.
        /// </summary>
        public int QuestionTypeId { get; set; }

        public Question()
        {
            Variants = new List<Variant>();
        }
    }
}
