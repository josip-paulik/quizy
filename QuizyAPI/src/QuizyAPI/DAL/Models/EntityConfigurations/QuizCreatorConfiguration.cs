﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace QuizyAPI.DAL.Models.EntityConfigurations
{
    public class QuizCreatorConfiguration : IEntityTypeConfiguration<QuizCreator>
    {
        public void Configure(EntityTypeBuilder<QuizCreator> builder)
        {
            builder.HasKey(x => new { x.ProfessorId, x.QuizId });

            builder.HasOne(x => x.Quiz)
                .WithMany(x => x.QuizCreators)
                .HasForeignKey(x => x.QuizId);

            builder.HasOne(x => x.Professor)
                .WithMany(x => x.QuizzesCreated)
                .HasForeignKey(x => x.ProfessorId);

        }
    }
}
