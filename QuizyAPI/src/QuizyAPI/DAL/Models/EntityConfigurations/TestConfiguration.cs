﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace QuizyAPI.DAL.Models.EntityConfigurations
{
    public class TestConfiguration : IEntityTypeConfiguration<Test>
    {
        public void Configure(EntityTypeBuilder<Test> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Quiz)
                .WithMany(x => x.Tests)
                .HasForeignKey(x => x.QuizId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Professor)
                .WithMany(x => x.TestsInitated)
                .HasForeignKey(x => x.ProfessorId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(x => x.ProfessorId)
                .IsRequired();

            builder.HasOne(x => x.Group)
                .WithMany(x => x.Tests)
                .HasForeignKey(x => x.GroupId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(x => x.GroupId)
                .IsRequired();

            builder.Property(x => x.DateTimeInitiated)
                .IsRequired();
        }
    }
}
