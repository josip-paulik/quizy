﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace QuizyAPI.DAL.Models.EntityConfigurations
{
    public class VariantConfiguration : IEntityTypeConfiguration<Variant>
    {
        public void Configure(EntityTypeBuilder<Variant> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.QuestionText)
                .IsRequired();

            builder.HasOne(x => x.Question)
                .WithMany(x => x.Variants)
                .HasForeignKey(x => x.QuestionId);

            builder.Property(x => x.QuestionId)
                .IsRequired();
        }
    }
}
