﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace QuizyAPI.DAL.Models.EntityConfigurations
{
    public class StudentQuestionPerfomanceConfiguration : IEntityTypeConfiguration<StudentQuestionPerfomance>
    {
        public void Configure(EntityTypeBuilder<StudentQuestionPerfomance> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Student)
                .WithMany(x => x.StudentQuestionPerfomances)
                .HasForeignKey(x => x.StudentId);

            builder.Property(x => x.StudentId)
                .IsRequired();

            builder.HasOne(x => x.Variant)
                .WithMany(x => x.StudentQuestionPerfomances)
                .HasForeignKey(x => x.VariantId);

            builder.HasOne(x => x.Test)
                .WithMany(x => x.StudentQuestionPerfomances);
        }
    }
}
