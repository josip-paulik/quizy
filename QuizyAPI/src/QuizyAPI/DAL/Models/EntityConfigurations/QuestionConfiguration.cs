﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace QuizyAPI.DAL.Models.EntityConfigurations
{
    public class QuestionConfiguration : IEntityTypeConfiguration<Question>
    {
        public void Configure(EntityTypeBuilder<Question> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Index)
                .IsRequired();

            builder.Property(x => x.MaximumPoints)
                .IsRequired();

            builder.Property(x => x.TimeToAnswerInSeconds)
                .IsRequired();

            builder.HasOne(x => x.Quiz)
                .WithMany(x => x.Questions)
                .HasForeignKey(x => x.QuizId);

            builder.Property(x => x.QuizId)
                .IsRequired();

            builder.Property(x => x.QuestionTypeId)
                .IsRequired();
        }
    }
}
