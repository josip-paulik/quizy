﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace QuizyAPI.DAL.Models.EntityConfigurations
{
    public class AnswerConfiguration : IEntityTypeConfiguration<Answer>
    { 
        public void Configure(EntityTypeBuilder<Answer> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Text)
                .IsRequired();

            builder.Property(x => x.IsCorrect)
                .IsRequired();

            builder.HasOne(x => x.Variant)
                .WithMany(x => x.Answers)
                .HasForeignKey(x => x.VariantId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Property(x => x.VariantId)
                .IsRequired();
        }
    }
}
