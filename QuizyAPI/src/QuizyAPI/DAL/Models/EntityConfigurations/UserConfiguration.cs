﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace QuizyAPI.DAL.Models.EntityConfigurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.UniqueAcademicId)
                .IsRequired();
            builder.HasIndex(x => x.UniqueAcademicId)
                .IsUnique();

            builder.Property(x => x.InstallationId)
                .IsRequired();

            builder.Property(x => x.EmailAddress)
                .IsRequired();
            builder.HasIndex(x => x.EmailAddress)
                .IsUnique();

            builder.Property(x => x.RoleId)
                .IsRequired();
        }
    }
}
