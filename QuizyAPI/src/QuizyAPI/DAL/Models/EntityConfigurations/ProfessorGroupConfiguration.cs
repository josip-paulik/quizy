﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace QuizyAPI.DAL.Models.EntityConfigurations
{
    public class ProfessorGroupConfiguration : IEntityTypeConfiguration<ProfessorGroup>
    {
        public void Configure(EntityTypeBuilder<ProfessorGroup> builder)
        {
            builder.HasKey(x => new { x.ProfessorId, x.GroupId });

            builder.HasOne(x => x.Group)
                .WithMany(x => x.ProfessorGroups)
                .HasForeignKey(x => x.GroupId);

            builder.HasOne(x => x.Professor)
                .WithMany(x => x.GroupsLeading)
                .HasForeignKey(x => x.ProfessorId);
        }
    }
}
