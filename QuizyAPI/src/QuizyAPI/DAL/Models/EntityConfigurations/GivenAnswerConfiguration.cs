﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace QuizyAPI.DAL.Models.EntityConfigurations
{
    public class GivenAnswerConfiguration : IEntityTypeConfiguration<GivenAnswer>
    {
        public void Configure(EntityTypeBuilder<GivenAnswer> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.StudentQuestionPerfomanceId)
                .IsRequired();

            builder.HasOne(x => x.StudentQuestionPerfomance)
                .WithMany(x => x.GivenAnswers)
                .HasForeignKey(x => x.StudentQuestionPerfomanceId);

            builder.Property(x => x.StudentQuestionPerfomanceId)
                .IsRequired();

            builder.Property(x => x.GivenAnswerText)
                .IsRequired();
        }
    }
}
