﻿using System;
using System.Collections.Generic;

namespace QuizyAPI.DAL.Models
{ 
    public class Variant
    {
        /// <summary>
        /// Unique identifer
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Text of this variant for the question it is in.
        /// </summary>
        public string QuestionText { get; set; }

        /// <summary>
        /// Id of the question variant is in.
        /// </summary>
        public string QuestionId { get; set; }

        /// <summary>
        /// Question this variant belongs to.
        /// </summary>
        public Question Question { get; set; }

        /// <summary>
        /// All answers this variant has(for multiple choice or free text questions).
        /// Free text questions have only one answer which is correct.
        /// </summary>
        public List<Answer> Answers { get; set; }

        /// <summary>
        /// Student question perfomances for this variant.
        /// </summary>
        public List<StudentQuestionPerfomance> StudentQuestionPerfomances { get; set; }

        public Variant()
        {
            Answers = new List<Answer>();
            StudentQuestionPerfomances = new List<StudentQuestionPerfomance>();
        }
    }
}
