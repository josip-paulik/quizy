﻿using System;
namespace QuizyAPI.DAL.Models
{
    public class QuizCreator
    {
        /// <summary>
        /// Id of the quiz a professor helped cocreate.
        /// </summary>
        public int QuizId { get; set; }

        /// <summary>
        /// Quiz a professor helped cocreate.
        /// </summary>
        public Quiz Quiz { get; set; }

        /// <summary>
        /// Professor who was cocreator of a quiz<see cref="QuizCreator.Quiz"/>.(identifier)
        /// </summary>
        public int ProfessorId { get; set; }

        /// <summary>
        /// Professor who was cocreator of a quiz<see cref="QuizCreator.Quiz"/>.
        /// </summary>
        public User Professor { get; set; }
    }
}
