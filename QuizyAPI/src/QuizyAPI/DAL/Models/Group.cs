﻿using System;
using System.Collections.Generic;

namespace QuizyAPI.DAL.Models
{
    public class Group
    {
        /// <summary>
        /// Unique identifier for group
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the group
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Professors associated with groups
        /// </summary>
        public List<ProfessorGroup> ProfessorGroups { get; set; }

        /// <summary>
        /// Student associated with groups.
        /// </summary>
        public List<StudentGroup> StudentGroups { get; set; }

        /// <summary>
        /// All quizzes this group has solved.
        /// </summary>
        public List<Test> Tests { get; set; }

        public Group()
        {
            StudentGroups = new List<StudentGroup>();
            Tests = new List<Test>();
            ProfessorGroups = new List<ProfessorGroup>();
        }
    }
}
