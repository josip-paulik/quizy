﻿using System;
namespace QuizyAPI.DAL.Models
{
    public class StudentGroup
    {
        /// <summary>
        /// Student who belongs to some group.(identifier)
        /// </summary>
        public int StudentId { get; set; }

        /// <summary>
        /// Student who belongs to some group.
        /// </summary>
        public User Student { get; set; }

        /// <summary>
        /// Group in which student is in.(identifier)
        /// </summary>
        public int GroupId { get; set; }

        /// <summary>
        /// Group in which student is in.
        /// </summary>
        public Group Group { get; set; }
    }
}
