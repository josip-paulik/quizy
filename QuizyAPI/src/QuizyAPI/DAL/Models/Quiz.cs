﻿using System;
using System.Collections.Generic;

namespace QuizyAPI.DAL.Models
{
    public class Quiz
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Title of the quiz
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Summary of quiz(what is it about)
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// List of professors who cocreated quiz.
        /// </summary>
        public List<QuizCreator> QuizCreators { get; set; }

        /// <summary>
        /// Questions that quiz contains.
        /// </summary>
        public List<Question> Questions { get; set; }

        /// <summary>
        /// All the tests that were initiated with this quiz.
        /// </summary>
        public List<Test> Tests { get; set; }

        public Quiz()
        {
            Questions = new List<Question>();
            QuizCreators = new List<QuizCreator>();
            Tests = new List<Test>();
        }
    }
}
