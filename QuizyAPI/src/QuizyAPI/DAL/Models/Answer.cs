﻿using System;
namespace QuizyAPI.DAL.Models
{
    public class Answer
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Answer text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Is answer correct
        /// </summary>
        public bool IsCorrect { get; set; }

        /// <summary>
        /// Id of the variant this answer is in.
        /// </summary>
        public string VariantId { get; set; }

        /// <summary>
        /// Variant this question is in.
        /// </summary>
        public Variant Variant { get; set; }
    }
}
