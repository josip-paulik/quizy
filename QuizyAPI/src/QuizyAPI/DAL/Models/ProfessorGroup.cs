﻿using System;
namespace QuizyAPI.DAL.Models
{
    /// <summary>
    /// Holds data of which professors run which group.
    /// </summary>
    public class ProfessorGroup
    {
        /// <summary>
        /// Professor who leads this group.(identifer)
        /// </summary>
        public int ProfessorId { get; set; }

        /// <summary>
        /// Professor who leads this group.
        /// </summary>
        public User Professor { get; set; }

        /// <summary>
        /// Group which professor leads.(identifier)
        /// </summary>
        public int GroupId { get; set; }

        /// <summary>
        /// Group which professor leads.
        /// </summary>
        public Group Group { get; set; }
    }
}
