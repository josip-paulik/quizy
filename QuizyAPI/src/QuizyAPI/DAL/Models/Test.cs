﻿using System;
using System.Collections.Generic;

namespace QuizyAPI.DAL.Models
{
    public class Test
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Quiz that that was tested.(identifer)
        /// </summary>
        public int? QuizId { get; set; }

        /// <summary>
        /// Quiz that was tested.
        /// </summary>
        public Quiz Quiz { get; set; }

        /// <summary>
        /// Professor that initated this test(identifier).
        /// </summary>
        public int ProfessorId { get; set; }

        /// <summary>
        /// Professor that initated this test.
        /// </summary>
        public User Professor { get; set; }

        /// <summary>
        /// Group that took this test(identifier).
        /// </summary>
        public int GroupId { get; set; }

        /// <summary>
        /// Group that took this test.
        /// </summary>
        public Group Group { get; set; }

        /// <summary>
        /// Time when the test weas initated.
        /// </summary>
        public DateTime DateTimeInitiated { get; set; }

        /// <summary>
        /// All question perfomances during this test.
        /// </summary>
        public List<StudentQuestionPerfomance> StudentQuestionPerfomances { get; set; }

        public Test()
        {
            StudentQuestionPerfomances = new List<StudentQuestionPerfomance>();
        }
    }
}
