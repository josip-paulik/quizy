﻿using System;
using System.Collections.Generic;

namespace QuizyAPI.Repositories
{
    /// <summary>
    /// This interface models basic behaviour of a repository.
    /// </summary>
    /// <typeparam name="T">Type of entity which is modified</typeparam>
    public interface IGenericRepository<T> where T : class
    {
        /// <summary>
        /// Adds new entity into its corresponding table.
        /// </summary>
        /// <param name="entity">Entity to be added</param>
        void Add(T entity);

        /// <summary>
        /// Gets entites(with projection modified by <paramref name="selector"/>)
        /// from database that match <paramref name="predicate"/>, paged.
        /// </summary>
        /// <param name="predicate">Predicate by which entities will be filtered.</param>
        /// <param name="selector">Selector for projection.</param>
        /// <param name="page">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Projected entities</returns>
        IEnumerable<Type> Filter<Type>(
            Func<T, bool> predicate,
            Func<T, Type> selector,
            int page = 1,
            int pageSize = 20);

        /// <summary>
        /// Updates entity in database.
        /// </summary>
        /// <param name="entity">Entity to be updated</param>
        void Update(T entity);

        /// <summary>
        /// Deletes entity from database.
        /// </summary>
        /// <param name="entity">Entity to be deleted.</param>
        void Delete(T entity);
    }
}
