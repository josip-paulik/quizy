﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using QuizyAPI.DAL;
using QuizyAPI.DAL.Models;

namespace QuizyAPI.Repositories
{
    public class QuizRepository : GenericRepository<Quiz>, IQuizRepository
    {
        public QuizRepository(ApplicationContext context) : base(context)
        {
        }

        /// <summary>
        /// Get quiz details for provided quiz id.
        /// </summary>
        /// <param name="quizId">Quiz id</param>
        /// <returns>Quiz with additional details</returns>
        public Quiz GetQuizDetails(int quizId)
        {
            var quiz = context.Quizzes.AsNoTracking()
                .Include(q => q.Questions)
                    .ThenInclude(q => q.Variants)
                    .ThenInclude(v => v.Answers)
                .Where(x => x.Id == quizId)
                .FirstOrDefault();

            return quiz;
        }
    }
}
