﻿using System;
using System.Collections.Generic;
using QuizyAPI.DAL.Models;

namespace QuizyAPI.Repositories
{
    public interface ITestRepository : IGenericRepository<Test>
    {
        /// <summary>
        /// Gets attendatnce checks for provided group identifier.
        /// </summary>
        /// <param name="groupId">Group identifier</param>
        /// <returns>Attendance checks for provided group identifier.</returns>
        List<Test> GetAttendanceChecksForGroup(int groupId);
    }
}
