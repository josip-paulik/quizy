﻿using System;
using QuizyAPI.DAL.Models;

namespace QuizyAPI.Repositories
{
    public interface IQuizRepository : IGenericRepository<Quiz>
    {
        /// <summary>
        /// Get quiz details for provided quiz id.
        /// </summary>
        /// <param name="quizId">Quiz id</param>
        /// <returns>Quiz with additional details</returns>
        Quiz GetQuizDetails(int quizId);
    }
}
