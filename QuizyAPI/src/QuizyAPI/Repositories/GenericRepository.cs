﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using QuizyAPI.DAL;

namespace QuizyAPI.Repositories
{
    /// <summary>
    /// Generic repository which defines basic behaviour
    /// for repositories.
    /// </summary>
    /// <typeparam name="T">Type of entity(representing table)</typeparam>
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly ApplicationContext context;

        public GenericRepository(ApplicationContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Adds new entity into its corresponding table.
        /// </summary>
        /// <param name="entity">Entity to be added</param>
        public void Add(T entity)
        {
            var dataSet = context.Set<T>();

            dataSet.Add(entity);
            context.SaveChanges();
        }

        /// <summary>
        /// Gets entites(with projection modified by <paramref name="selector"/>)
        /// from database that match <paramref name="predicate"/>, paged.
        /// </summary>
        /// <param name="predicate">Predicate by which entities will be filtered.</param>
        /// <param name="selector">Selector for projection.</param>
        /// <param name="page">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Projected entities</returns>
        public IEnumerable<Type> Filter<Type>(
            Func<T, bool> predicate,
            Func<T, Type> selector,
            int page = 1,
            int pageSize = 20)
        {
            var dataSet = context.Set<T>().AsNoTracking();

            var result = dataSet
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .Where(predicate)
                .Select(selector)
                .ToList();

            return result;
        }

        /// <summary>
        /// Updates entity in database.
        /// </summary>
        /// <param name="entity">Entity to be updated</param>
        public void Update(T entity)
        {
            var dataSet = context.Set<T>();

            dataSet.Update(entity);
            context.SaveChanges();
        }

        /// <summary>
        /// Deletes entity from database.
        /// </summary>
        /// <param name="entity">Entity to be deleted.</param>
        public void Delete(T entity)
        {
            var dataSet = context.Set<T>();

            dataSet.Remove(entity);
            context.SaveChanges();
        }
    }
}
