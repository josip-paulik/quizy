﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using QuizyAPI.DAL;
using QuizyAPI.DAL.Models;
using QuizyAPI.Utility;

namespace QuizyAPI.Repositories
{
    public class StudentQuestionPerfomanceRepository : GenericRepository<StudentQuestionPerfomance>, IStudentQuestionPerfomanceRepository
    {
        public StudentQuestionPerfomanceRepository(ApplicationContext context) : base(context)
        {
        }

        /// <summary>
        /// Get student's perfomances on all tests he's taken.
        /// </summary>
        /// <param name="studentId">Student identifier</param>
        /// <returns>Student's perfomances on tests he's taken.</returns>
        public List<StudentQuestionPerfomance> GetStudentPerfomanceDetails(int studentId, int testId)
        {
            var studentPerfomance = context.StudentQuestionPerfomances
                .AsNoTracking()
                .Include(x => x.GivenAnswers)
                .Include(x => x.Variant)
                    .ThenInclude(x => x.Answers)
                .Include(x => x.Variant)
                    .ThenInclude(x => x.Question)
                .Where(x =>
                    x.StudentId == studentId
                    && x.VariantId != null
                    && x.VariantId != string.Empty
                    && x.TestId == testId)
                .ToList();

            return studentPerfomance;
        }

        /// <summary>
        /// Gets student perfomance data for one test.
        /// </summary>
        /// <param name="studentId">Student identifier</param>
        /// <param name="testId">Test identifier</param>
        /// <returns>Student perfomance data for one test</returns>
        public List<StudentQuestionPerfomance> GetStudentPerfomances(int studentId)
        {
            var allStudentPerfomances = context.StudentQuestionPerfomances
                .AsNoTracking()
                .Include(x => x.GivenAnswers)
                .Include(x => x.Variant)
                    .ThenInclude(x => x.Answers)
                .Include(x => x.Variant)
                    .ThenInclude(x => x.Question)
                .Include(x => x.Test)
                    .ThenInclude(x => x.Quiz)
                        .ThenInclude(x => x.Questions)
                .Where(x => x.StudentId == studentId && x.VariantId != null && x.VariantId != string.Empty)
                .ToList();

            return allStudentPerfomances;
        }
    }
}
