﻿using System;
using QuizyAPI.DAL.Models;

namespace QuizyAPI.Repositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
    }
}
