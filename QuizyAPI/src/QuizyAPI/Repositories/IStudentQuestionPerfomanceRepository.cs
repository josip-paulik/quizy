﻿using System;
using System.Collections.Generic;
using QuizyAPI.DAL.Models;

namespace QuizyAPI.Repositories
{
    public interface IStudentQuestionPerfomanceRepository : IGenericRepository<StudentQuestionPerfomance>
    {
        /// <summary>
        /// Get student's perfomances on all tests he's taken.
        /// </summary>
        /// <param name="studentId">Student identifier</param>
        /// <returns>Student's perfomances on tests he's taken.</returns>
        List<StudentQuestionPerfomance> GetStudentPerfomances(int studentId);

        /// <summary>
        /// Gets student perfomance data for one test.
        /// </summary>
        /// <param name="studentId">Student identifier</param>
        /// <param name="testId">Test identifier</param>
        /// <returns>Student perfomance data for one test</returns>
        List<StudentQuestionPerfomance> GetStudentPerfomanceDetails(int studentId, int testId);
    }
}
