﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using QuizyAPI.DAL;
using QuizyAPI.DAL.Models;

namespace QuizyAPI.Repositories
{
    public class TestRepository : GenericRepository<Test>, ITestRepository
    {
        public TestRepository(ApplicationContext context) : base(context)
        {
        }

        /// <summary>
        /// Gets attendatnce checks for provided group identifier.
        /// </summary>
        /// <param name="groupId">Group identifier</param>
        /// <returns>Attendance checks for provided group identifier.</returns>
        public List<Test> GetAttendanceChecksForGroup(int groupId)
        {
            var attendanceChecks = context.Tests
                .AsNoTracking()
                .Include(x => x.StudentQuestionPerfomances)
                    .ThenInclude(x => x.Student)
                .Where(x => x.GroupId == groupId && (x.QuizId == null || x.QuizId == 0))
                .ToList();

            return attendanceChecks;
        }
    }
}
