﻿using System;
using QuizyAPI.DAL;
using QuizyAPI.DAL.Models;

namespace QuizyAPI.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
