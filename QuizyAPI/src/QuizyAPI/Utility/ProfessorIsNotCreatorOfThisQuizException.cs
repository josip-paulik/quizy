﻿using System;
using System.Runtime.Serialization;

namespace QuizyAPI.Utility
{
    [Serializable]
    internal class ProfessorIsNotCreatorOfThisQuizException : Exception
    {
        public ProfessorIsNotCreatorOfThisQuizException()
        {
        }

        public ProfessorIsNotCreatorOfThisQuizException(string message) : base(message)
        {
        }

        public ProfessorIsNotCreatorOfThisQuizException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ProfessorIsNotCreatorOfThisQuizException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}