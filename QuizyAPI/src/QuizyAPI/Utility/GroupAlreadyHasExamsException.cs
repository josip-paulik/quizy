﻿using System;
using System.Runtime.Serialization;

namespace QuizyAPI.Utility
{
    [Serializable]
    internal class GroupAlreadyHasExamsException : Exception
    {
        public GroupAlreadyHasExamsException()
        {
        }

        public GroupAlreadyHasExamsException(string message) : base(message)
        {
        }

        public GroupAlreadyHasExamsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected GroupAlreadyHasExamsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}