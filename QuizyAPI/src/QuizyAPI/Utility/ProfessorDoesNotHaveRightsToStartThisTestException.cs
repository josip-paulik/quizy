﻿using System;
using System.Runtime.Serialization;

namespace QuizyAPI.Utility
{
    [Serializable]
    internal class ProfessorDoesNotHaveRightsToStartThisTestException : Exception
    {
        public ProfessorDoesNotHaveRightsToStartThisTestException()
        {
        }

        public ProfessorDoesNotHaveRightsToStartThisTestException(string message) : base(message)
        {
        }

        public ProfessorDoesNotHaveRightsToStartThisTestException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ProfessorDoesNotHaveRightsToStartThisTestException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}