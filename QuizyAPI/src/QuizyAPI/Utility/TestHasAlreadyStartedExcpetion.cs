﻿using System;
using System.Runtime.Serialization;

namespace QuizyAPI.Utility
{
    [Serializable]
    internal class TestHasAlreadyStartedException : Exception
    {
        public TestHasAlreadyStartedException()
        {
        }

        public TestHasAlreadyStartedException(string message) : base(message)
        {
        }

        public TestHasAlreadyStartedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected TestHasAlreadyStartedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}