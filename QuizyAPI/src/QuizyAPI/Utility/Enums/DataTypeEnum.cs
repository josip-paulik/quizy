﻿using System;
namespace QuizyAPI.Utility.Enums
{
    /// <summary>
    /// Enumerates types of data which answers can have.
    /// </summary>
    public enum DataTypeEnum
    {
        Integer = 1,
        FloatingPointNumber = 2,
        Text = 3
    }

    /// <summary>
    /// Class used for returning data type as a string.
    /// </summary>
    public static class DataTypeString
    {
        /// <summary>
        /// Returns data type as string based on provided <paramref name="dataTypeEnum"/>.
        /// </summary>
        /// <param name="dataTypeEnum">Data type enum</param>
        /// <returns>Data type name</returns>
        public static string GetDataTypeName(DataTypeEnum dataTypeEnum)
        {
            return dataTypeEnum switch
            {
                DataTypeEnum.Integer => "Integer",
                DataTypeEnum.FloatingPointNumber => "Floating point number",
                DataTypeEnum.Text => "Text",
                _ => string.Empty
            };
        }
    }
}
