﻿using System;
namespace QuizyAPI.Utility.Enums
{
    /// <summary>
    /// Enumerates user roles in Quizy system.
    /// </summary>
    public enum RoleEnum
    {
        Professor = 1,
        Student = 2
    }

    /// <summary>
    /// Class used for returning role as a string.
    /// </summary>
    public static class RoleString
    {
        /// <summary>
        /// Returns role as string based on provided <paramref name="roleEnum"/>.
        /// </summary>
        /// <param name="roleEnum">Role enum</param>
        /// <returns>Role name</returns>
        public static string GetRoleName(RoleEnum roleEnum)
        {
            return roleEnum switch
            {
                RoleEnum.Professor => "Professor",
                RoleEnum.Student => "Student",
                _ => string.Empty
            };
        }
    }
}
