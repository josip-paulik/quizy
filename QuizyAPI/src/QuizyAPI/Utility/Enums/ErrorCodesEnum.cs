﻿using System;
namespace QuizyAPI.Utility.Enums
{
    /// <summary>
    /// This enum assignes number to various errors
    /// that can happen on an API request.
    /// </summary>
    public enum ErrorCodesEnum
    {
        UserAlreadyExists = 1,
        BadRequest = 2,
        UserDoesNotExist = 3,
        GroupAlreadyHasExams = 4,
        TestWithAccessCodeDoesNotExist = 5,
        ProfessorDoesNotHaveRightsToStartTest = 6,
        ProfessorIsNotCreatorOfThisQuiz = 7,
        QuizNotFound = 8,
        GroupNotFound = 9,
        ProfessorIsNotLeadingGroup = 10,
        TestNotFound = 11,
        StudentIsInTestAlready = 12,
        StudentIsNotInGroup = 13,
        TestHasAlreadyStarted = 14
    }

    /// <summary>
    /// This class is used to get error message assigned to a error code.
    /// </summary>
    public static class ErrorCodeMessageGetter
    {
        /// <summary>
        /// Gets error message based on provided error code.
        /// </summary>
        /// <param name="errorCodesEnum">Provided error code</param>
        /// <returns>Error message</returns>
        public static string GetErrorCodeMessage(ErrorCodesEnum errorCodesEnum)
        {
            return errorCodesEnum switch
            {
                ErrorCodesEnum.UserAlreadyExists => "User already exists.",
                ErrorCodesEnum.BadRequest => "Bad request was sent. Reasons:",
                ErrorCodesEnum.UserDoesNotExist => "User does not exist. Are your credentials correct?",
                ErrorCodesEnum.GroupAlreadyHasExams => "Group already has exams.",
                ErrorCodesEnum.TestWithAccessCodeDoesNotExist => "Test with provided access code does not exist.",
                ErrorCodesEnum.ProfessorDoesNotHaveRightsToStartTest => "Professor does not have rights to start test.",
                ErrorCodesEnum.ProfessorIsNotCreatorOfThisQuiz => "Professor is not cocreator of this quiz.",
                ErrorCodesEnum.QuizNotFound => "Quiz not found.",
                ErrorCodesEnum.GroupNotFound => "Group not found",
                ErrorCodesEnum.ProfessorIsNotLeadingGroup => "Professor is not leading group.",
                ErrorCodesEnum.TestNotFound => "Test not found",
                ErrorCodesEnum.StudentIsInTestAlready => "Student is in test already.",
                ErrorCodesEnum.StudentIsNotInGroup => "Student is not in group.",
                ErrorCodesEnum.TestHasAlreadyStarted => "Test has already started.",
                _ => string.Empty
            };
        }
    }

}
