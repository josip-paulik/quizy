﻿using System;
namespace QuizyAPI.Utility.Enums
{
    /// <summary>
    /// Enumerates question types that question can have.
    /// </summary>
    public enum QuestionTypeEnum
    {
        Choice = 1,
        OpenText = 2
    }

    /// <summary>
    /// Class that is used to retrieve question type name.
    /// </summary>
    public static class QuestionTypeString
    {
        /// <summary>
        /// This method gets question type name based on provided enum value.
        /// </summary>
        /// <param name="questionTypeEnum">Question type</param>
        /// <returns>Question type name</returns>
        public static string GetQuestionTypeName(QuestionTypeEnum questionTypeEnum)
        {
            return questionTypeEnum switch
            {
                QuestionTypeEnum.Choice => "Choice",
                QuestionTypeEnum.OpenText => "OpenText",
                _ => string.Empty
            };
        }
    }
}
