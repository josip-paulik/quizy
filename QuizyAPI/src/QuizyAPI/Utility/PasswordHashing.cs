﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace QuizyAPI.Utility
{
    public static class PasswordHashing
    {
        /// <summary>
        /// Hashes password with random salt unless it is provided.
        /// </summary>
        /// <param name="password">Clear text password</param>
        /// <param name="salt">Salt</param>
        /// <returns>Hashed password</returns>
        public static string Hash(string password, string salt = null)
        {
            if (salt.IsNullOrEmpty())
            {
                using (var algorithm = new Rfc2898DeriveBytes(
                        password,
                        saltSize: 10,
                        iterations: 100,
                        HashAlgorithmName.SHA256))
                {
                    var key = Convert.ToBase64String(algorithm.GetBytes(10));

                    salt = Convert.ToBase64String(algorithm.Salt);

                    return $"{algorithm.IterationCount}.{salt}.{key}";
                }
            }

            else
            {
                using (var algorithm = new Rfc2898DeriveBytes(
                        password,
                        Convert.FromBase64String(salt),
                        iterations: 100,
                        HashAlgorithmName.SHA256))
                {
                    var key = Convert.ToBase64String(algorithm.GetBytes(10));

                    salt = Convert.ToBase64String(algorithm.Salt);

                    return $"{algorithm.IterationCount}.{salt}.{key}";
                }
            }
        }

        /// <summary>
        /// Checks if hashed password matches provided password.
        /// </summary>
        /// <param name="hash">Hashed password</param>
        /// <param name="password">Clear text, provided password</param>
        /// <returns>True if passwords match, false otherwise</returns>
        public static bool Check(string hash, string password)
        {
            var parts = hash.Split(".");
            var salt = parts[1];

            var hashedPassword = Hash(password, salt);

            return hashedPassword == hash;
        }
    }
}
