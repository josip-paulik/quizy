﻿using System;
using System.Runtime.Serialization;

namespace QuizyAPI.Utility
{
    [Serializable]
    internal class ProfessorIsNotLeadingGroupException : Exception
    {
        public ProfessorIsNotLeadingGroupException()
        {
        }

        public ProfessorIsNotLeadingGroupException(string message) : base(message)
        {
        }

        public ProfessorIsNotLeadingGroupException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ProfessorIsNotLeadingGroupException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}