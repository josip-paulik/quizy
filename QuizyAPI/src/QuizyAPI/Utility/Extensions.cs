﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Caching.Distributed;

namespace QuizyAPI.Utility
{
    public static class Extensions
    {
        /// <summary>
        /// Checks if list is null or empty.
        /// </summary>
        /// <typeparam name="T">Object type of list element</typeparam>
        /// <param name="enumerable">List that is checked</param>
        /// <returns>True if list is null or empty.</returns>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            var returnValue = enumerable == null || enumerable.Count() == 0;

            return returnValue;
        }

        /// <summary>
        /// Get error message from ModelState which contains errors for object if object had them.
        /// </summary>
        /// <param name="modelStateDictionary">ModelState</param>
        /// <returns>Error message composed from errors found in ModelState</returns>
        public static string GetErrorMessage(this ModelStateDictionary modelStateDictionary)
        {
            var errorMessage = string.Empty;
            foreach (var modelState in modelStateDictionary.Values)
            {
                foreach (var error in modelState.Errors)
                {
                    errorMessage = $"{errorMessage} - {error.ErrorMessage}";
                }
            }

            return errorMessage;
        }

        /// <summary>
        /// Gets user id from claims(in this application, inside jwt token).
        /// </summary>
        /// <param name="user">User with its claims data.</param>
        /// <returns>User id</returns>
        public static int GetUserId(this ClaimsPrincipal user)
        {
            var userIdString = user.Claims
                .Where(x => x.Type == ClaimTypes.Name)
                .First()
                .Value;

            var userId = Convert.ToInt32(userIdString);

            return userId;
        }

        /// <summary>
        /// Gets object from cache.
        /// </summary>
        /// <typeparam name="T">Type of object to be retrieved from cache.</typeparam>
        /// <param name="distributedCache">Cache that is worked with.</param>
        /// <param name="key">Key where the entry is located.</param>
        /// <returns>Value for key if it exists.</returns>
        public static T GetObjectFromCache<T>(this IDistributedCache distributedCache, string key)
        {
            var valueString = distributedCache.GetString(key);
            if (valueString.IsNullOrEmpty())
            {
                return default;
            }
            var value = JsonSerializer.Deserialize<T>(valueString);

            return value;
        }

        /// <summary>
        /// Sets object in cache.
        /// </summary>
        /// <typeparam name="T">Type of object to be set in cache.</typeparam>
        /// <param name="distributedCache">Cache that is worked with</param>
        /// <param name="key">Key for the entry in cache.</param>
        /// <param name="newValue">Value that the entry in cache will be set to.</param>
        public static void SetObjectInCache<T>(this IDistributedCache distributedCache, string key, T newValue)
        {
            var newValueString = JsonSerializer.Serialize(newValue);
            distributedCache.SetString(key, newValueString);
        }

    }
}
