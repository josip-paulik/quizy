﻿using System;
using System.Runtime.Serialization;

namespace QuizyAPI.Utility
{
    [Serializable]
    internal class StudentIsInTestAlreadyException : Exception
    {
        public StudentIsInTestAlreadyException()
        {
        }

        public StudentIsInTestAlreadyException(string message) : base(message)
        {
        }

        public StudentIsInTestAlreadyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected StudentIsInTestAlreadyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}