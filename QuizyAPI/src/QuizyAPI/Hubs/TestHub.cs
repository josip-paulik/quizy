﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Distributed;
using QuizyAPI.DAL.Models;
using QuizyAPI.Repositories;
using QuizyAPI.Services;
using QuizyAPI.Utility;

namespace QuizyAPI.Hubs
{
    public class TestHub : Hub
    {
        private readonly IDistributedCache distributedCache;
        private readonly ITestService testService;
        private readonly IGenericRepository<StudentQuestionPerfomance> studentQuestionPerfomanceRepository;

        public TestHub(
            ITestService testService,
            IDistributedCache distributedCache,
            IGenericRepository<StudentQuestionPerfomance> studentQuestionPerfomanceRepository)
        {
            this.testService = testService;
            this.distributedCache = distributedCache;
            this.studentQuestionPerfomanceRepository = studentQuestionPerfomanceRepository;
        }

        /// <summary>
        /// Joins the test with provided access code.
        /// </summary>
        /// <param name="accessCode">Access code</param>
        [Authorize(Roles = "2")]
        public async Task JoinTest(string accessCode)
        {
            var userId = Context.User.GetUserId();

            var currentTestState = distributedCache.GetObjectFromCache<CurrentTestState>(accessCode);

            if (currentTestState == null || !currentTestState.EntryAllowed)
            {
                return;
            }

            currentTestState.StudentsJoined.Add(userId);
            currentTestState.ConnectionIds.Add(Context.ConnectionId);
            distributedCache.SetObjectInCache(accessCode, currentTestState);

            await Groups.AddToGroupAsync(Context.ConnectionId, accessCode);
            await Clients.Group(accessCode)
                .SendAsync("TestLobbyUpdated", currentTestState.StudentsJoined.Count);

        }

        /// <summary>
        /// Submits answers and saves those in database.
        /// Then when all students have answered on question, proceeds to next one
        /// or ends test, depending if there are questions left.
        /// </summary>
        /// <param name="accessCode">Access code for the test</param>
        /// <param name="variantId">Variant that the student got</param>
        /// <param name="answers">Answers that student submitted</param>
        [Authorize(Roles = "2")]
        public async Task SubmitAnswer(string accessCode, string variantId, params string[] answers)
        {
            var studentId = Context.User.GetUserId();

            var currentTestState = distributedCache.GetObjectFromCache<CurrentTestState>(accessCode);

            if (currentTestState == null
                || currentTestState.StudentSubmittedAnswerOnCurrentQuestion.Contains(studentId))
            {
                return;
            }

            currentTestState.StudentSubmittedAnswerOnCurrentQuestion.Add(studentId);
            distributedCache.SetObjectInCache(accessCode, currentTestState);

            var newPerfomanceId = Guid.NewGuid().ToString();

            var givenAnswers = !answers.IsNullOrEmpty() ?
                answers
                    .Select(x => new GivenAnswer
                    {
                        Id = Guid.NewGuid().ToString(),
                        GivenAnswerText = x,
                        StudentQuestionPerfomanceId = newPerfomanceId
                    })
                    .ToList()
                : new List<GivenAnswer>();

            var studentPerfomance = new StudentQuestionPerfomance
            {
                GivenAnswers = givenAnswers,
                StudentId = studentId,
                TestId = currentTestState.TestId,
                VariantId = variantId,
                Id = newPerfomanceId
            };

            studentQuestionPerfomanceRepository.Add(studentPerfomance);

            currentTestState = distributedCache.GetObjectFromCache<CurrentTestState>(accessCode);

            var totalNumberOfStudents = currentTestState.StudentsJoined.Count();
            var numberOfStudentSubmittedAnswers = currentTestState.StudentSubmittedAnswerOnCurrentQuestion.Count();

            await Clients.Group(accessCode)
                .SendAsync("AnswerSubmitted", numberOfStudentSubmittedAnswers, totalNumberOfStudents);

            if (totalNumberOfStudents == numberOfStudentSubmittedAnswers)
            {
                currentTestState.CurrentQuestionIndex++;
                currentTestState.StudentSubmittedAnswerOnCurrentQuestion.Clear();

                if (currentTestState.CurrentQuestionIndex > currentTestState.Questions.Count())
                {
                    await Clients.Group(accessCode).SendAsync("TestEnded");

                    foreach (var connectionId in currentTestState.ConnectionIds)
                    {
                        await Groups.RemoveFromGroupAsync(connectionId, accessCode);
                    }

                    var groupsTakingTestKey = "GroupsTakingTest";
                    var groupsTakingTestList = distributedCache.GetObjectFromCache<List<int>>(groupsTakingTestKey);

                    if (groupsTakingTestList.Contains(currentTestState.GroupId))
                    {
                        groupsTakingTestList.Remove(currentTestState.GroupId);
                        distributedCache.SetObjectInCache(groupsTakingTestKey, groupsTakingTestList);
                    }

                    distributedCache.Remove(accessCode);
                }
                else
                {
                    distributedCache.SetObjectInCache(accessCode, currentTestState);

                    var currentQuestion = currentTestState.GetCurrentQuestion();

                    foreach (var connectionId in currentTestState.ConnectionIds)
                    {
                        var chosenVariant = currentTestState.GetRandomVariantForCurrentQuestion();

                        await Clients.Client(connectionId)
                            .SendAsync("PrepareNextQuestion",
                                currentTestState.CurrentQuestionIndex,
                                currentQuestion.TimeToAnswerInSeconds,
                                currentQuestion.QuestionType,
                                chosenVariant);
                    }
                }
            }
        }

        [Authorize(Roles = "2")]
        public async Task ExitTest(string accessCode)
        {
            var studentId = Context.User.GetUserId();
            
            var currentTestState = distributedCache.GetObjectFromCache<CurrentTestState>(accessCode);

            if (currentTestState == null)
            {
                return;
            }

            currentTestState.StudentsJoined.Remove(studentId);
            currentTestState.ConnectionIds.Remove(Context.ConnectionId);

            distributedCache.SetObjectInCache(accessCode, currentTestState);

            var totalNumberOfStudents = currentTestState.StudentsJoined.Count();
            var numberOfStudentSubmittedAnswers = currentTestState.StudentSubmittedAnswerOnCurrentQuestion.Count();

            await Clients.Group(accessCode)
                .SendAsync("AnswerSubmitted", numberOfStudentSubmittedAnswers, totalNumberOfStudents);

            if (totalNumberOfStudents == numberOfStudentSubmittedAnswers)
            {
                currentTestState.CurrentQuestionIndex++;
                currentTestState.StudentSubmittedAnswerOnCurrentQuestion.Clear();

                if (currentTestState.CurrentQuestionIndex > currentTestState.Questions.Count())
                {
                    await Clients.Group(accessCode).SendAsync("TestEnded");

                    foreach (var connectionId in currentTestState.ConnectionIds)
                    {
                        await Groups.RemoveFromGroupAsync(connectionId, accessCode);
                    }

                    var groupsTakingTestKey = "GroupsTakingTest";
                    var groupsTakingTestList = distributedCache.GetObjectFromCache<List<int>>(groupsTakingTestKey);

                    if (groupsTakingTestList.Contains(currentTestState.GroupId))
                    {
                        groupsTakingTestList.Remove(currentTestState.GroupId);
                        distributedCache.SetObjectInCache(groupsTakingTestKey, groupsTakingTestList);
                    }

                    distributedCache.Remove(accessCode);
                }
                else
                {
                    distributedCache.SetObjectInCache(accessCode, currentTestState);

                    var currentQuestion = currentTestState.GetCurrentQuestion();

                    foreach (var connectionId in currentTestState.ConnectionIds)
                    {
                        var chosenVariant = currentTestState.GetRandomVariantForCurrentQuestion();

                        await Clients.Client(connectionId)
                            .SendAsync("PrepareNextQuestion",
                                currentTestState.CurrentQuestionIndex,
                                currentQuestion.TimeToAnswerInSeconds,
                                currentQuestion.QuestionType,
                                chosenVariant);
                    }
                }
            }
        }
    }
}
