﻿using System;
using System.Collections.Generic;

namespace QuizyAPI.Controllers.ViewModels
{
    public class AttendanceModel
    {
        /// <summary>
        /// When was attendance check done.
        /// </summary>
        public DateTime DateTimeInitiated { get; set; }

        /// <summary>
        /// Full names of students that were present
        /// during attendance check.
        /// </summary>
        public List<string> FullNames { get; set; }
    }
}
