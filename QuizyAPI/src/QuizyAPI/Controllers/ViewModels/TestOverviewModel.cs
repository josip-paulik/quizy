﻿using System;
namespace QuizyAPI.Controllers.ViewModels
{
    /// <summary>
    /// Model that is returned upon starting the lobby.
    /// </summary>
    public class TestOverviewModel
    {
        /// <summary>
        /// Test identifier, used to later start the test.
        /// </summary>
        public int TestId { get; set; }

        /// <summary>
        /// Access code so professor can share it with students.
        /// </summary>
        public string AccessCode { get; set; }
    }
}
