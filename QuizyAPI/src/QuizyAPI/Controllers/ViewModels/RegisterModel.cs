﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QuizyAPI.Controllers.ViewModels
{
    public class RegisterModel
    {
        /// <summary>
        /// Email address
        /// </summary>
        [Required(ErrorMessage = "emailAddress is required.")]
        [EmailAddress(ErrorMessage = "Email adress is in wrong format.")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Full name
        /// </summary>
        [Required(ErrorMessage = "fullName is required.")]
        public string FullName { get; set; }

        /// <summary>
        /// Unique academic id.
        /// </summary>
        [Required(ErrorMessage = "uniqueAcademicId is required.")]
        public string UniqueAcademicId { get; set; }

        /// <summary>
        /// Installation id(password).
        /// </summary>
        [Required(ErrorMessage = "installationId is required.")]
        public string InstallationId { get; set; }

        /// <summary>
        /// Role user will have
        /// </summary>
        [Required(ErrorMessage = "role is required.")]
        public int Role { get; set; }
    }
}
