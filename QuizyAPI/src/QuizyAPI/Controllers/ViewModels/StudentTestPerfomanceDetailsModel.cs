﻿using System;
using System.Collections.Generic;

namespace QuizyAPI.Controllers.ViewModels
{
    /// <summary>
    /// This class describes perfomance on a single question
    /// student was solving.
    /// </summary>
    public class StudentTestPerfomanceDetailsModel
    {
        /// <summary>
        /// Index by which questions are ordered.
        /// </summary>
        public int QuestionIndex { get; set; }

        /// <summary>
        /// Text of the question student got.
        /// </summary>
        public string QuestionText { get; set; }

        /// <summary>
        /// What were correct answers.
        /// </summary>
        public List<string> CorrectAnswers { get; set; }

        /// <summary>
        /// Answers that student provided.
        /// </summary>
        public List<string> GivenAnswers { get; set; }

        /// <summary>
        /// Points that student scored
        /// </summary>
        public float PointsScored { get; set; }

        /// <summary>
        /// Points that student could have scored.
        /// </summary>
        public float MaximumPoints { get; set; }

        /// <summary>
        /// Answers that were offered(if question was of choice type).
        /// </summary>
        public List<string> OfferedAnswers { get; set; }
    }
}
