﻿using System;
using QuizyAPI.Utility.Enums;

namespace QuizyAPI.Controllers.ViewModels
{
    public class ErrorModel
    {
        /// <summary>
        /// Error code from <see cref="ErrorCodesEnum">.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Error message for a certain error code.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets error model prepared to return as response to request,
        /// </summary>
        /// <param name="errorCodeEnum">Error code on which error model will be created.</param>
        /// <returns>Error model made from error code.</returns>
        public static ErrorModel GetErrorModelFromErrorCodeEnum(ErrorCodesEnum errorCodeEnum)
        {
            var errorModel = new ErrorModel
            {
                ErrorCode = (int)errorCodeEnum,
                ErrorMessage = ErrorCodeMessageGetter.GetErrorCodeMessage(errorCodeEnum)
            };

            return errorModel;
        }
    }
}
