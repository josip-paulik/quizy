﻿using System;
namespace QuizyAPI.Controllers.ViewModels
{
    /// <summary>
    /// Overview of student's test perfomance.
    /// </summary>
    public class StudentTestPerfomanceOverviewModel
    {
        /// <summary>
        /// Which test was taken.
        /// </summary>
        public int TestId { get; set; }

        /// <summary>
        /// Title of the quiz that was taken.
        /// </summary>
        public string QuizTitle { get; set; }

        /// <summary>
        /// Summary of the quiz that was taken.
        /// </summary>
        public string QuizSummary { get; set; }

        /// <summary>
        /// How many points were scored on the test.
        /// </summary>
        public float PointsScored { get; set; }

        /// <summary>
        /// How many points student could have scored.
        /// </summary>
        public float MaxPoints { get; set; }

        /// <summary>
        /// When the test started.
        /// </summary>
        public DateTime DateTimeInitiated { get; set; }
    }
}
