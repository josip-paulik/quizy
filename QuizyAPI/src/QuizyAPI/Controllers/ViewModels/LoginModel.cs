﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QuizyAPI.Controllers.ViewModels
{
    public class LoginModel
    {
        /// <summary>
        /// Email address(like username).
        /// </summary>
        [Required(ErrorMessage = "emailAddress is required.")]
        [EmailAddress(ErrorMessage = "Email adress is in wrong format.")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Installation id like password.
        /// </summary>
        [Required(ErrorMessage = "installationId is required.")]
        public string InstallationId { get; set; }
    }
}
