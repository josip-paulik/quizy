﻿using System;
namespace QuizyAPI.Controllers.ViewModels
{
    public class TokenModel
    {
        public string Token { get; set; }
    }
}
