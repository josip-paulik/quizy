﻿using System;
using Microsoft.AspNetCore.Mvc;
using QuizyAPI.Utility.Enums;
using QuizyAPI.DAL.Models;
using QuizyAPI.Controllers.ViewModels;
using QuizyAPI.Services;
using QuizyAPI.Utility;
using Microsoft.AspNetCore.Authorization;

namespace QuizyAPI.Controllers
{
    /// <summary>
    /// Controller is used for any requests regarding users.
    /// </summary>
    [Route("user")]
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IStudentQuestionPerfomanceService perfomanceService;
        public UserController(
            IUserService userService,
            IStudentQuestionPerfomanceService perfomanceService)
        {
            this.userService = userService;
            this.perfomanceService = perfomanceService;
        }

        /// <summary>
        /// Registers new user.
        /// </summary>
        /// <param name="registerModel">Model containing new user data.</param>
        /// <returns>Token which is used for further requests.</returns>
        [HttpPost("register")]
        public IActionResult Register([FromBody]RegisterModel registerModel)
        {

            if (!ModelState.IsValid)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.BadRequest);
                errorModel.ErrorMessage = $"{errorModel.ErrorMessage} {ModelState.GetErrorMessage()}";
                return BadRequest(errorModel);
            }

            var newUser = userService.Register(registerModel);
            if (newUser == null)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.UserAlreadyExists);

                return Conflict(errorModel);
            }

            var tokenModel = new TokenModel
            {
                Token = userService.GetToken(newUser)
            };
            
            return Ok(tokenModel);
        }

        /// <summary>
        /// Tries to login to application using provided credentials.
        /// </summary>
        /// <param name="loginModel">Provided credentials</param>
        /// <returns>Token if login was successful.</returns>
        [HttpPost("login")]
        public IActionResult Login([FromBody]LoginModel loginModel)
        {
            if (!ModelState.IsValid)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.BadRequest);
                errorModel.ErrorMessage = $"{errorModel.ErrorMessage} {ModelState.GetErrorMessage()}";
                return BadRequest(errorModel);
            }

            var loggedInUser = userService.TryLogin(loginModel);
            if (loggedInUser != null)
            {
                var token = userService.GetToken(loggedInUser);
                var tokenModel = new TokenModel
                {
                    Token = token
                };
                return Ok(tokenModel);
            }
            else
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.UserDoesNotExist);
                return NotFound(errorModel);
            }
        }

        /// <summary>
        /// Gets all test perfomances for a student.
        /// </summary>
        /// <returns>All test perfomances for student.</returns>
        [HttpGet("student/testPerfomance")]
        [Authorize(Roles = "2")]
        public IActionResult GetTestPerfomance()
        {
            var studentId = HttpContext.User.GetUserId();
            var studentPerfomanceOverview = perfomanceService.GetStudentPerfomanceOverview(studentId);

            return Ok(studentPerfomanceOverview);
        }

        /// <summary>
        /// Gets test perfomance for one test that was taken.
        /// </summary>
        /// <param name="testId">Test id for which perfomance will be loaded.</param>
        /// <returns>Test perfomance details for a test.</returns>
        [HttpGet("student/testPerfomanceDetails")]
        [Authorize(Roles = "2")]
        public IActionResult GetTestPerfomanceDetails([FromQuery]int testId)
        {
            if (testId == 0)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.BadRequest);
                errorModel.ErrorMessage = $"{errorModel.ErrorMessage} - testId param is required.";
                return BadRequest(errorModel);
            }

            var studentId = HttpContext.User.GetUserId();
            var studentPerfomanceDetails = perfomanceService.GetStudentPerfomanceDetails(studentId, testId);

            if (studentPerfomanceDetails.IsNullOrEmpty())
            {
                return NotFound();
            }

            return Ok(studentPerfomanceDetails);
        }

    }
}
