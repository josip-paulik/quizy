﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuizyAPI.Controllers.ViewModels;
using QuizyAPI.Services;
using QuizyAPI.Utility;
using QuizyAPI.Utility.Enums;

namespace QuizyAPI.Controllers
{
    /// <summary>
    /// Controller is used for any requests regarding tests.
    /// </summary>
    [Route("test")]
    public class TestController : Controller
    {
        private readonly ITestService testService;

        public TestController(ITestService testService)
        {
            this.testService = testService;
        }

        /// <summary>
        /// Starts test(inserts new state into cache).
        /// And prepares lobby for test.
        /// </summary>
        /// <param name="quizId">Which quiz to start(if not provided, attendance check will be run).</param>
        /// <param name="groupId">For which group to start a lobby.</param>
        /// <returns>If successful, returns access code and test id.</returns>
        [Authorize(Roles = "1")]
        [HttpPost("startLobby")]
        public IActionResult StartTestLobby([FromQuery]int? quizId, [FromQuery]int groupId)
        {
            if (groupId == 0)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.BadRequest);
                errorModel.ErrorMessage = $"{errorModel.ErrorMessage} groupId param must be submitted";
                return BadRequest(errorModel);
            }

            var professorId = HttpContext.User.GetUserId();
            var testModel = new TestOverviewModel();
            try
            {
                testModel = testService.StartTestLobby(professorId, quizId, groupId);
            }
            catch (GroupAlreadyHasExamsException ex)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.GroupAlreadyHasExams);
                return Conflict(errorModel);
            }
            catch (ProfessorIsNotCreatorOfThisQuizException ex)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.ProfessorIsNotCreatorOfThisQuiz);
                return StatusCode(403, errorModel);
            }
            catch (ProfessorIsNotLeadingGroupException ex)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.ProfessorIsNotLeadingGroup);
                return StatusCode(403, errorModel);
            }
            catch (QuizNotFoundException ex)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.QuizNotFound);
                return NotFound(errorModel);
            }
            catch (GroupNotFoundException ex)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.GroupNotFound);
                return NotFound(errorModel);
            }
            

            return Ok(testModel);
        }

        /// <summary>
        /// Tries to put student into test with provided access code.
        /// </summary>
        /// <param name="accessCode">Access code</param>
        /// <returns>Returns OK if it is possible to join test, 404 otherwise.</returns>
        [Authorize(Roles = "2")]
        [HttpGet("canJoin")]
        public IActionResult CanJoinTest([FromQuery]string accessCode)
        {
            if (accessCode.IsNullOrEmpty())
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.BadRequest);
                errorModel.ErrorMessage = $"{errorModel.ErrorMessage} accessCode param must be submitted";
                return BadRequest(errorModel);
            }
            var studentId = HttpContext.User.GetUserId();
            var canJoinTest = false;
            try
            {
                canJoinTest = testService.CanJoinTest(studentId, accessCode);
            }
            catch (TestNotFoundException ex)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.TestNotFound);
                return NotFound(errorModel);
            }
            catch (StudentIsInTestAlreadyException ex)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.StudentIsInTestAlready);
                return Conflict(errorModel);
            }

            if (canJoinTest)
            {
                return Ok();
            }
            else
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.StudentIsNotInGroup);
                return StatusCode(403, errorModel);
            }
        }

        /// <summary>
        /// Starts test which has provided access code.
        /// </summary>
        /// <param name="accessCode">Provided access code.</param>
        /// <returns>OK if test has successfully started.</returns>
        [Authorize(Roles = "1")]
        [HttpPut("start")]
        public IActionResult StartTest([FromQuery]string accessCode)
        {
            var professorId = HttpContext.User.GetUserId();
            try
            {
                testService.StartTest(professorId, accessCode);
            }
            catch (ProfessorDoesNotHaveRightsToStartThisTestException ex)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.ProfessorDoesNotHaveRightsToStartTest);
                return StatusCode(403, errorModel);
            }
            catch (TestNotFoundException ex)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.TestWithAccessCodeDoesNotExist);
                return NotFound(errorModel);
            }
            catch (TestHasAlreadyStartedException ex)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.TestHasAlreadyStarted);
                return Conflict(errorModel);
            }
            

            return Ok();
        }

        /// <summary>
        /// Gets all attendance checks for given group.
        /// </summary>
        /// <param name="groupId">Group</param>
        /// <returns>All attendance checks and which students participated in which.</returns>
        [HttpGet("attendance")]
        [Authorize(Roles = "1")]
        public IActionResult GetAttendanceRecordsForGroup([FromQuery]int groupId)
        {
            if (groupId == 0)
            {
                var errorModel = ErrorModel.GetErrorModelFromErrorCodeEnum(ErrorCodesEnum.BadRequest);
                errorModel.ErrorMessage = $"{errorModel.ErrorMessage} - groupId param is required.";
                return BadRequest(errorModel);
            }

            var attendanceChecks = testService.GetAttendancesForGroup(groupId);

            return Ok(attendanceChecks);
        }
    }
}
