using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using QuizyAPI.DAL;
using QuizyAPI.Hubs;
using QuizyAPI.Repositories;
using QuizyAPI.Services;
using StackExchange.Redis;

namespace QuizyAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = true;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["Jwt:Key"])),
                    ValidIssuer = Configuration["Jwt:Issuer"],
                    ValidateIssuer = true,
                    ValidAudience = Configuration["Jwt:Audience"],
                    ValidateAudience = true
                };
            });


            services.AddDbContext<ApplicationContext>((optionsBuilder) =>
            {
                optionsBuilder.UseSqlServer(
                    Configuration["ConnectionStrings:DefaultConnection"],
                    providerOptions => providerOptions.CommandTimeout(60))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            });

            ConfigureDependencies(services);

            services.AddSignalR();
            services.AddDistributedRedisCache(x =>
            {
                x.InstanceName = "master";
                x.ConfigurationOptions = new ConfigurationOptions
                {
                    AbortOnConnectFail = false,
                    EndPoints = { Configuration["Redis:ConnectionString"] }
                };
            });
        }

        private void ConfigureDependencies(IServiceCollection services)
        {
            // Repositories
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped(typeof(IUserRepository), typeof(UserRepository));
            services.AddScoped(typeof(IQuizRepository), typeof(QuizRepository));
            services.AddScoped(typeof(ITestRepository), typeof(TestRepository));
            services.AddScoped(typeof(IStudentQuestionPerfomanceRepository), typeof(StudentQuestionPerfomanceRepository));

            // Services
            services.AddScoped(typeof(IUserService), typeof(UserService));
            services.AddScoped(typeof(ITestService), typeof(TestService));
            services.AddScoped(typeof(IStudentQuestionPerfomanceService), typeof(StudentQuestionPerfomanceService));


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<TestHub>("/testHub");
            });
        }
    }
}
